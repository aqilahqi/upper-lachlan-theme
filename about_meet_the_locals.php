<?php include("header.php") ?>
<?php include("template-parts/partials/inner-page-slider-banner.php");?>

<!-- Breadcrumb -->
<nav class="breadcrumb border-b">
    <div class="container">
        <ul>
            <li><a href="/">Home</a></li>
            <li class="active">Breadcrumb</li>
        </ul>
    </div>
</nav>
<!-- Breadcrumb: END -->

<section class="about meet-locals">
    <div class="container">
        <div class="section-header">
            <h2 class="display">Meet the Locals</h2>
            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod.</p>
        </div>
        <div class="panel-wrapper">
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body title">
                    <div class="body-title text-center">
                        <h3 class="display">The name or names of the Locals</h3>
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
        </div>
        <div class="row justify-content-center mb-3">
            <a href="#" class="theme-btn">Load More</a>
        </div>
    </div>
</section>
<?php include("template-parts/partials/newsletter.php");?>
<?php include("footer.php") ?>