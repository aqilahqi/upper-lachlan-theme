<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div>
        <div class="inner-page-body twin">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <img src="assets/images/about-history-post.png" alt="about-history" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="display">Lorem ipsum dolor sit amet</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
            <div class="groups">
                <panel class="plain">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <h3 class="display">Weddings, Parties and Meetings</h3>
                        </div>
                        <div class="body-copy">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="see-more-wrapper">
                                <a href="#"><i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="plain">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <h3 class="display">Suggested Itineraries</h3>
                        </div>
                        <div class="body-copy">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="see-more-wrapper">
                                <a href="#"><i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
            </div>
        </div>
    </div>
</section>


<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>