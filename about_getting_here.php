<?php include("header.php") ?>
<div class="map-accordian-section bg-theme-light-brown" style="background-image: url('assets/images/svg_map/svg-map-bg.png');">
    <div class="container">
        <div class="row justify-content-center">
            <div class="svg-map">
                <div class="main-map">
                    <?php echo file_get_contents("assets/images/svg_map/UL_Interactive_Map.svg"); ?> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb -->
<nav class="breadcrumb border-b bg-theme-light">
    <div class="container">
        <ul>
            <li><a href="/">Home</a></li>
            <li class="active">Breadcrumb</li>
        </ul>
    </div>
</nav>
<!-- Breadcrumb: END -->

<section class="about getting-here bg-theme-light">
    <div class="container">
        <div class="section-header">
            <h1 class="display">Getting Here</h1>
            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. <a href="#">Click here for Getting Around</a> information.</p>
        </div>
        <div class="section-body">
            <nav class="nav-tabs-wrapper">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link display active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Driving</a>
                    <a class="nav-item nav-link display" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Flying</a>
                    <a class="nav-item nav-link display" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Public Transport</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 order-md-2"><div id="location-map" class="driving-map"></div></div>
                        <div class="col-xs-12 col-md-6">
                            <h3 class="display">Getting to Upper Lachlan by Road</h3>
                            <p><b>The Upper Lachlan Shire</b> can be accessed via rail with stations at <b>Goulburn</b> and <b>Gunning</b>. If arriving in Goulburn, travellers can catch the <b>Crookwell Bus Service</b> that runs between <b>Goulburn and Crookwell 3</b> times a day.</p>
                            <p>For the latest train times please follow this link to the <b>Country Link timetable - <a href="#">click here</a></b></p>
                            <p>For the latest  <b>Crookwell Bus Service timetable - <a href="#">click here</a></b></p>
                            <p><b>Murray's Coaches</b> also have a regular service to <b>Goulburn - <a href="#">click here</a></b></p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 order-md-2"><img src="assets/images/tab-1.png" alt="" class="img-fluid"></div>
                        <div class="col-xs-12 col-md-6">
                            <h3 class="display">Flying to Upper Lachlan</h3>
                            <p><b>The Upper Lachlan Shire</b> can be accessed via rail with stations at <b>Goulburn</b> and <b>Gunning</b>. If arriving in Goulburn, travellers can catch the <b>Crookwell Bus Service</b> that runs between <b>Goulburn and Crookwell 3</b> times a day.</p>
                            <p>For the latest train times please follow this link to the <b>Country Link timetable - <a href="#">click here</a></b></p>
                            <p>For the latest  <b>Crookwell Bus Service timetable - <a href="#">click here</a></b></p>
                            <p><b>Murray's Coaches</b> also have a regular service to <b>Goulburn - <a href="#">click here</a></b></p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 order-md-2"><img src="assets/images/tab-1.png" alt="" class="img-fluid"></div>
                        <div class="col-xs-12 col-md-6">
                            <h3 class="display">Public Transport</h3>
                            <p><b>The Upper Lachlan Shire</b> can be accessed via rail with stations at <b>Goulburn</b> and <b>Gunning</b>. If arriving in Goulburn, travellers can catch the <b>Crookwell Bus Service</b> that runs between <b>Goulburn and Crookwell 3</b> times a day.</p>
                            <p>For the latest train times please follow this link to the <b>Country Link timetable - <a href="#">click here</a></b></p>
                            <p>For the latest  <b>Crookwell Bus Service timetable - <a href="#">click here</a></b></p>
                            <p><b>Murray's Coaches</b> also have a regular service to <b>Goulburn - <a href="#">click here</a></b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("location-map"), mapOptions);
    // map.setTilt(45);
    var faIcon = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#2B2F3D',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    var faIcon_click = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#A1855B',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    // Multiple Markers
    var markers = [
        ['Laggan, NSW', -34.3461707, 149.5000172],
        ['Grassmere Park, NSW', -34.7446904,149.688714]
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<panel class="plain map"><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
        
        ['<panel class="plain map"><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: faIcon,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                // infowindow.close();
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
                var gm = jQuery('.gm-style-iw').parent();
                var gm_ = jQuery('.gm-style-iw').prev();
                var gm_close = jQuery('.gm-style-iw').next();
                var gm_arrow = gm_.children().eq(2);
                var gm_white = gm_.children().last();
                var gm_shadow = gm_arrow.prev();
                var gm_arrow_shadow = gm_shadow.prev();
                gm.addClass('parent-info-window');
                gm_.addClass('wrapper-info-window');
                gm_arrow.addClass('wrapper-arrow');
                gm_white.addClass('wrapper-white');
                gm_shadow.addClass('wrapper-shadow');
                gm_arrow_shadow.addClass('arrow-shadow');
                gm_close.addClass('close-window');
                // for (var i = 0; i < infoWindowContent.length; i++ ) {  //I assume you have your infoboxes in some array
                //     infoWindowContent[i][0].close();
                // }
            }
        })(marker, i));

        // infowindow.open(map);
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon_click); 
            }
        })(marker, i));

        // on mouseout (moved mouse off marker) make infoWindow disappear
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon); 
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });

}


</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>
<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>