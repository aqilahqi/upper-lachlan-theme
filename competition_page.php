<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>
<section class="inner-page bg-theme-light ">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div>
        <div class="inner-page-body twin">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <img src="assets/images/about-history-post.png" alt="about-history" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="display">Lorem ipsum dolor sit amet</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="inner-page default">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h4 class="display">Terms and conditions of the competition</h2>
        </div>
        <div class="inner-page-body">
            <ol>
                <li><p>Information on how to enter form part of the terms and conditions of entry. Entry into the competition is deemed acceptance of these terms and conditions.</p></li>
                <li><p>The promoter is the Upper Lachlan Shire Council, ABN 81 011 241 552, 36 Goulburn St, Crookwell NSW 2583, ph (02) 4832 1988.</p></li>
                <li><p>Entry is open to all permanent residents of Australia. Employees and their immediate families of the Upper Lachlan Shire Council, Lindner Quality Socks and the Upper Lachlan Tourist Association Committee are ineligible to enter the competition. Multiple entries are permitted.</p></li>
                <li><p>The competition commences at 9.00am (AEST) on Monday 4 September 2017 and closes at 11.59pm (AEST) on Saturday 30 September 2017.</p></li>
                <li><p>How to enter:</p>
                    <p>Step 1: Take and edit your photo</p>
                    <p>Step 2: Upload the photo to Instagram with the tags @upplachtourism and #capturemycountry (Instagram privacy settings must be set to ‘public’ for us to retrieve your photo)</p>
                    <p>Step 3: Follow Upper Lachlan Tourism’s Instagram account “upplachtourism”</p>
                </li>
                <li><p>Only entries submitted via the Instagram application and which are appropriately hashtagged will be accepted. Photographs submitted must be original works.</p></li>
                <li><p>Judging will take place on Tuesday 10 October 2017 at 3pm (AEST) at the Crookwell Visitor Information Centre. If the winner is under the age of 18 years, the prize will be awarded to their parent or legal guardian.</p></li>
                <li><p>The competition is a game of skill and chance plays no part in the selection of the winner. Submissions will be judged by the Upper Lachlan Tourist Association Committee and tourism staff of the Upper Lachlan Shire Council.</p></li>
                <li><p>Judge’s decision is final and no correspondence will be entered into. Prizes cannot be transferred. Non-cash prizes cannot be redeemed for cash.</p></li>
                <li><p>The promoter reserves the right to verify the validity of entries and to disqualify any entry which, in the opinion of promoter, includes objectionable content, profanity, potentially insulting, inflammatory or defamatory statements, disqualify any entrant who tampers with the entry process, who submits an entry that is not in accordance with these terms and conditions of entry or who has, in the opinion of promoter, engaged in conduct in entering the promotion which is fraudulent, misleading, deceptive or generally damaging to the goodwill or reputation of the promotion and/or promoter. The promoter reserves the right to disqualify a winner if the promoter becomes aware that the winner and/or the winner’s entry is of a type described in this clause.</p></li>
                <li><p>Entries must be uploaded by 11.59pm (AEST) Saturday 30 September 2017. The time of entry will in each case be the time the online entry is visible to the promoter on Instagram. The promoter accepts no responsibility for any late, lost or misdirected entries due to technical disruptions, network congestion or for any other reason.</p></li>
                <li>
                    <p>Use of images:</p>
                    <ol>
                        <li><p>All images published on Instagram with #capturemycountry may be moderated and displayed on the website and social media channels of the promoter and its associated partners. These images and associated usernames will be public to all.</p></li>
                        <li><p>By entering the competition and accepting the terms and conditions you agree that you hereby license the rights to use your photo submission to the Upper Lachlan Shire Council and its associated partners on any of its websites or social media channels, or for advertising and promotion of its products, services, and/or its company in any media now known or hereafter developed in perpetuity in exchange for no compensation of any kind.</p></li>
                        <li><p>By entering the competition and accepting the terms and conditions you agree that you hereby license the rights to use your photo submission to the Upper Lachlan Shire Council for the production of products for sale in exchange for no compensation of any kind to the entrant.</p></li>
                    </ol>
                    
                </li>
                <li><p>Use of personal information:</p>
                    <ol>
                        <li><p>By entering the competition and accepting the terms and conditions, you agree that the promoter may use your personal details for the purpose of conducting the competition, including the use of your Instagram name in promotional formats.</p></li>
                        <li><p>By entering the competition, you give the promoter permission to contact you via Instagram if you are a winner of the competition for the purpose of distributing your prize to you.</p></li>
                    </ol>
                </li>
                <li><p>The promoter reserves the right to request winners to provide proof of identity and proof of residency at the nominated prize delivery address. Identification considered suitable for verification is at the discretion of the promoter.</p></li>
                <li><p>The promoter shall endeavour to contact the winners via their Instagram account within one week of the judging. In the event that the winner/s cannot be contacted after one month, an alternate winner/s will be selected and the original winner/s will forfeit any prize.</p></li>
                <li><p>The promoter shall not be liable for any loss or damage whatsoever which is suffered (including but not limited to direct or consequential loss) or for any personal injury suffered or sustained in connection with any prize/s, except for any liability which cannot be excluded by law.</p></li>
                <li><p>The promoter is not responsible for any incorrect or inaccurate information, either caused by the entrant or for any of the equipment or programming associated with or utilised in this competition, or for any technical error, or any combination thereof that may occur in the course of the administration of this competition including any omission, interruption, deletion, defect, delay in operation or transmission, communications line or satellite network failure, theft or destruction or unauthorised access to or alteration of entries.</p></li>
                <li><p>If for any reason this competition is not capable of running as planned, including due to infection by computer virus, bugs, tampering, unauthorised intervention, fraud, technical failures or any causes beyond the control of the promoter, which corrupt or affect the administration, security, fairness or integrity or proper conduct of this promotion, the promoter reserves the right in its sole discretion to disqualify any individual who tampers with the entry process, take any action that may be available, and to cancel, terminate, modify or suspend the competition, subject to any written direction given under state regulation.</p></li>
                <li>
                    <p>Prizes are as follows:</p>
                    <p>First prize is $150 voucher, second prize is a $75 voucher and third prize is a $25 voucher for goods from Lindner Quality Socks which can be redeemed instore or online. The voucher will be mailed or emailed to the winner once a postal address or email address has been verified. The voucher is not redeemable for cash, any portion unused within a twelve month period from the date of issue will be forfeited.</p>
                </li>
                <li><p>Participation in this competition assumes the acceptance of the Instagram terms and conditions, found at <a href="#">http://instagram.com/legal/terms/</a></p></li>
                <li><p>The promotion is in no way sponsored, endorsed or administered by, or associated with, Instagram or Facebook</p></li>
            </ol>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>