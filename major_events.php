<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>


<section class="featured-listing major-events">
    <div class="container-fluid">
        <div class="section-header">
            <h2 class="display">Featured Listings</h2>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dignissimos voluptates iste nihil ratione, deleniti quidem minima nobis repellat maxime vel rerum animi harum quibusdam necessitatibus porro quos sed dolores incidunt.</p>
        </div>
        <div class="panel-wrapper d-md-flex">
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>