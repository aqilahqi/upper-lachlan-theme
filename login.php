<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light login-portal">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">You will need a username and password in order to enter the Industry Portal. If you have already registered please enter your details below, if you require access, please fill in the sign up form to request access.</h2>
        </div>
        <div class="inner-page-body">
            <div class="portal-wrapper">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><span>Login</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><span>Sign up</span></a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="login-wrapper">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username</label>
                                    <input type="text" class="form-control" id="loginUsername">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="loginPassword">
                                </div>
                                <button type="submit" class="theme-btn">Submit</button>
                            </form>
                            <a href="#" class="forgot-password">Forgot Password?</a>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="signup-wrapper">
                            <form>
                                <div class="form-group">
                                    <label>First Name<sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Last Name<sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Phone Numbere<sup>*</sup></label>
                                    <input type="number" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" class="form-control">
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">I agree to <a href="#">Upper Lachlan's Terms</a></label>
                                </div>
                                <button type="submit" class="theme-btn">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>