<?php include("header.php") ?>
<div class="page-wrapper" style="background-image:url(assets/images/bg-about.jpg);">
    <div class="container">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->

        <!-- Panel section -->
        <section class="about panel-wrapper">
            <panel class="plain">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <h3 class="display">History</h3>
                    </div>
                    <div class="body-copy">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#"><i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="plain">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <h3 class="display">History</h3>
                    </div>
                    <div class="body-copy">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#"><i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="plain">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <h3 class="display">History</h3>
                    </div>
                    <div class="body-copy">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#"><i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
        </section>
        <!-- Panel section: END -->

        <!-- Collage section -->
        <section class="about collage-wrapper">
            <a class="collage-item w-img" style="background-image: url('assets/images/post-6.png');">
                <div class="copy-wrapper">
                    <span>Live, Work &amp; Invest</span>
                </div>
            </a>
            <a class="collage-item" style="background-color:#768A3A;">
                 <div class="copy-wrapper">
                    <span>Seasons</span>
                 </div>
            </a>
            <a class="collage-item w-img" style="background-image: url('assets/images/collage-5.jpg');">
                <div class="copy-wrapper">
                    <span>Maps</span>
                </div>
            </a>
            <a class="collage-item" style="background-color:#A1855B;">
                <div class="copy-wrapper">
                    <span>Getting Here</span>
                </div>
            </a>
            <a class="collage-item w-img" style="background-image: url('assets/images/post-7.png');">
                <div class="copy-wrapper">
                    <span>Tablelands</span>
                </div>
            </a>
            <a class="collage-item" style="background-color:#2B2F3D;">
                <div class="copy-wrapper">
                    <span>Getting Around</span>
                </div>
            </a>
        </section>
        <!-- Collage section: END -->
    </div>
</div>

<?php include("template-parts/partials/newsletter.php");?>
<?php include("footer.php") ?>