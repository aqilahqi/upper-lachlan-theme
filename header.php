<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#2B2F3D">
    <link rel="stylesheet" href="assets/css/main.css">
    <title>Upper Lachlan</title>
</head>
<body>

<?php include("template-parts/components/sidebar-left.php");?>

<?php include("template-parts/components/navbar-main.php");?>