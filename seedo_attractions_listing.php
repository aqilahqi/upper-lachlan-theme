<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page listings">
    <div class="container">
        <!-- <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div> -->
        <div class="inner-page-body">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <img src="assets/images/about-live-post.png" alt="about-history" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <!-- <h3 class="display">Lorem ipsum dolor sit amet</h3> -->
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
            <div class="row justify-content-end">
                <div class="share-post">
                    <div class="col-12">
                    <p>Share</p>
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="far fa-share-square"></i></a></li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inner-page facts details bg-theme-light">
    <div class="container">
        <div class="section-header">
            <h2 class="display dark">Details</h2>
        </div>
        <ul class="details-wrapper">
            <li>
                <?php echo file_get_contents("assets/images/icons/phone.svg"); ?> 
                <p>PHONE</p>
                <h5 class="w-link"><a href="tel:0277778888">02 7777 8888</a></h5>
            </li>
            <li>
                <?php echo file_get_contents("assets/images/icons/Email.svg"); ?> 
                <p>EMAIL</p>
                <h5 class="w-link"><a href="mailto:info@somewhere.com.au">info@somewhere.com.au</a></h5>
            </li>
            <li>
                <?php echo file_get_contents("assets/images/icons/Website.svg"); ?> 
                <p>WEBSITE</p>
                <h5 class="w-link"><a href="www.somesiteinaustralia.com">www.somesiteinaustralia.com</a></h5>
            </li>
            <li>
                <?php echo file_get_contents("assets/images/icons/phone.svg"); ?> 
                <p>PHONE</p>
                <h5 class="w-link"><a href="tel:0277778888">02 7777 8888</a></h5>
            </li>
            <li>
                <?php echo file_get_contents("assets/images/icons/Email.svg"); ?> 
                <p>EMAIL</p>
                <h5 class="w-link"><a href="mailto:info@somewhere.com.au">info@somewhere.com.au</a></h5>
            </li>
            <li>
                <p>FOLLOW US</p>
                <ul class="social">
                <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-google-plus-square"></i></a></li>
                </ul>
                <!-- <h5 class="w-link"><a href="www.somesiteinaustralia.com">www.somesiteinaustralia.com</a></h5> -->
            </li>
            <!-- <li>
                <?php echo file_get_contents("assets/images/icons/Website.svg"); ?> 
                <p>WEBSITE</p>
                <h5 class="w-link"><a href="www.somesiteinaustralia.com">www.somesiteinaustralia.com</a></h5>
            </li>
            <li>
                <?php echo file_get_contents("assets/images/icons/Website.svg"); ?> 
                <p>WEBSITE</p>
                <h5 class="w-link"><a href="www.somesiteinaustralia.com">www.somesiteinaustralia.com</a></h5>
            </li> -->
        </ul>
    </div>
</section>

<section class="location">
    <div class="bg-theme-dark">
        <div class="section-header">
            <h2 class="display">Location</h2>
        </div>
    </div>
</section>

<section class="location-map">

    <div id="location-map"></div>
    <div class="direction-map bg-theme-light">
        <h4 class="display">Title od the Product Listing</h4>
        <p>Some Building or number, Some St, Some Suburb or Town NSW 2586</p>
        <a href="#" class="theme-btn">Click for directions</a>
    </div>
</section>

<section class="location">
    <div class="">
        <div class="section-header">
            <h2 class="display black">Instagram</h2>
        </div>
    </div>
</section>

<?php include("template-parts/partials/social.php");?>


<section class="trip-advisor bg-theme-secondary">
    <div class="container">
        <img src="assets/images/logos/tripadvisor-logo.png" alt="tripadvisor" class="title img-fluid d-block mx-auto">
        <div class="row justify-content-between">
            <div class="col-xs-12 col-md-6">
                <div class="rating-wrapper d-lg-flex align-item-start justify-content-between">
                    <div class="rating-copy">
                        <h5>TRIPADVISOR TRAVELER RATING</h5>
                        <img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid d-block">
                    </div>
                    <a href="#" class="theme-btn">Read All Reviews</a>
                </div>
                <div class="review-wrapper d-lg-flex align-item-start justify-content-between">
                    <p>Tripadvisor rating based on <br> 151 reviews</p>
                    <a href="#" class="theme-btn">Write a Review</a>
                </div>
                <img src="assets/images/logos/tripadvisor-logo.png" alt="tripadvisor" class="img-fluid trip-copy-logo">
                <p>&copy; TripAdvisor 2018</p>
            </div>
            <div class="col-xs-12 col-md-6">
                <h5>TRIPADVISOR TRAVELER REVIEWS</h5>
                <div class="traveler-review">
                    <p>Title of Review</p>
                    <div class="review-header d-md-flex">
                        <img src="assets/images/logos/tripadvisor-ratings.png" alt="tripadvisor" class="img-fluid">
                        <p>Reviewed on 20 Sep 2017</p>
                    </div>
                    <div class="review-body">
                        <p>Lorem ipsum dolor sit amet, an utamur antiopam mel, eam nulla iusto praesent in, cu quaerendum suscipiantur nec. Mollis vivendo ius eu. Ne putant platonem mei, cum esse iudico altera.</p>
                    </div>
                    <div class="review-footer">
                        <p>Review by Bob Smith</p>
                        <p>Read <a href="#">full review</a> on TripAdvisor</p>
                    </div>
                </div>
                <div class="traveler-review">
                    <p>Title of Review</p>
                    <div class="review-header d-md-flex">
                        <img src="assets/images/logos/tripadvisor-ratings.png" alt="tripadvisor" class="img-fluid">
                        <p>Reviewed on 20 Sep 2017</p>
                    </div>
                    <div class="review-body">
                        <p>Lorem ipsum dolor sit amet, an utamur antiopam mel, eam nulla iusto praesent in, cu quaerendum suscipiantur nec. Mollis vivendo ius eu. Ne putant platonem mei, cum esse iudico altera.</p>
                    </div>
                    <div class="review-footer">
                        <p>Review by Bob Smith</p>
                        <p>Read <a href="#">full review</a> on TripAdvisor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="facilities">
    <div class="container">
        <div class="section-header">
            <h2 class="display black">Facilities</h2>
        </div>
        <ul class="facility-listings">
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
            <li>Facility items</li>
        </ul>
    </div>
</section>

<?php include("template-parts/partials/featured-listings.php");?>

<script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: -34.4574464, lng: 149.4676891};

  var faIcon = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#2B2F3D',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };

  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('location-map'), {zoom: 15, center: uluru});
      
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: faIcon
    });
}
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>
<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>