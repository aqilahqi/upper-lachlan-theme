
if(jQuery(".explore-map").length !== 0) {
    var waypoint= [];

    jQuery('panel.explore').each(function( index ) {
        // console.log( index + ": " + jQuery( this ).attr('data-ll') );
        waypoint.push({
        latitude: jQuery( this ).attr('data-lat'),
        longitude: jQuery( this ).attr('data-lng'),
        });
    });
    var last = waypoint.length - 1;

    console.log(waypoint[0].latitude);
    console.log('Last: ' + last);

}


function initMap() {

    /* Direction service */
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    var map;
    var bounds = new google.maps.LatLngBounds();
    var markers    = [];
    var mapOptions = {
        mapTypeId: 'roadmap',
    };
    directionsDisplay.setMap(map);

    /* Display a map on the page */
    map = new google.maps.Map(document.getElementById("location-map"), mapOptions);

    /* Route line */
    var polyline = new google.maps.Polyline({
        path: [],
        geodesic: true,
        strokeColor: '#466874',
        strokeOpacity: 1.0,
        strokeWeight: 6
    });

    /* Marker Icon */
    var faIcon = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#2B2F3D',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    var faIcon_click = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#A1855B',
        fillOpacity: 1,
        stroke:0,
        scale: .1,
        anchor: new google.maps.Point(200, 480),
    };
      
    // Info Window Content
    // var infoWindowContent = [
    //     ['<panel class="brown-tag map"><a href="#" class="header-wrapper"><div class="panel-header" style="background-image: url('+'assets/images/events-1.jpg'+');"><div class="desc-overlay"><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis aliquid odit?</p></div></div></a><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
        
    //     ['<panel class="brown-tag map"><a href="#" class="header-wrapper"><div class="panel-header" style="background-image: url('+'assets/images/events-1.jpg'+');"><div class="desc-overlay"><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis aliquid odit?</p></div></div></a><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
    // ];
        
    /* Display multiple markers on a map */
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    /* Loop through our array of markers & place each one on the map  */
    for( i = 0; i < waypoint.length; i++ ) {
        var position = new google.maps.LatLng(waypoint[i].latitude, waypoint[i].longitude);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: faIcon,
            // title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        // google.maps.event.addListener(marker, 'click', (function(marker, i) {
        //     return function() {
        //         // infowindow.close();
        //         infoWindow.setContent(infoWindowContent[i][0]);
        //         infoWindow.open(map, marker);
        //         var gm = jQuery('.gm-style-iw').parent();
        //         var gm_ = jQuery('.gm-style-iw').prev();
        //         var gm_close = jQuery('.gm-style-iw').next();
        //         var gm_arrow = gm_.children().eq(2);
        //         var gm_white = gm_.children().last();
        //         var gm_shadow = gm_arrow.prev();
        //         var gm_arrow_shadow = gm_shadow.prev();
        //         gm.addClass('parent-info-window');
        //         gm_.addClass('wrapper-info-window');
        //         gm_arrow.addClass('wrapper-arrow');
        //         gm_white.addClass('wrapper-white');
        //         gm_shadow.addClass('wrapper-shadow');
        //         gm_arrow_shadow.addClass('arrow-shadow');
        //         gm_close.addClass('close-window');
        //         // for (var i = 0; i < infoWindowContent.length; i++ ) {  //I assume you have your infoboxes in some array
        //         //     infoWindowContent[i][0].close();
        //         // }
        //     }
        // })(marker, i));

        // infowindow.open(map);

        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon_click); 
            }
        })(marker, i));

        /* on mouseout (moved mouse off marker) make infoWindow disappear */
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon); 
            }
        })(marker, i));

        /* Automatically center the map fitting all markers on the screen */
        // map.fitBounds(bounds);
        markers.push(marker);
        bounds.extend(marker.position);
        map.fitBounds(bounds);
        
    }

    if(markers.length) {

        var waypts = [];
        for (var i = 1; i < markers[last]; i++) {
            waypts.push({
                // location: new google.maps.LatLng(waypoint[i].latitude, waypoint[i].longitude),
                location: new google.maps.LatLng(markers[i].latitude, markers[i].longitude),
                // location: new google.maps.LatLng(waypoint[i].latitude, waypoint[i].longitude),
                stopover: true
            });
        }

        directionsService.route(
            {
                origin: markers[0].getPosition(),
                destination: markers[markers.length - 1].getPosition(),
                travelMode: google.maps.TravelMode.DRIVING,
                /* Modes: BICYCLING, WALKING, DRIVING */
                waypoints: waypts,
            },
            function(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    var legs = response.routes[0].legs;

                    for (var i = 0; i < legs.length; i++) {
                        var steps = legs[i].steps;

                        for (var j = 0; j < steps.length; j++) {
                            var nextSegment = steps[j].path;

                            for (var k = 0; k < nextSegment.length; k++) {
                                polyline.getPath().push(nextSegment[k]);
                                bounds.extend(nextSegment[k]);
                            }
                        }
                    }

                    polyline.setMap(map);
                } else {
                    console.warn('Directions request failed due to ' + status);
                }
            }
        );
          
    }


    /* Override our map zoom level once our fitBounds function runs (Make sure it only runs once) */
    // var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
    //     this.setZoom(8);
    //     google.maps.event.removeListener(boundsListener);
    // });

}