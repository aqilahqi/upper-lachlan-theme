<?php include("header.php") ?>
<?php include("template-parts/components/sidebar-right.php");?>
<link href="assets/css/datepicker.min.css" rel="stylesheet">

<div class="home-wrapper" style="background-image: url('assets/images/bg-home-gradient.jpg');">

    <div class="home-banner-content">
        <div class="container">
            <div class="home-slider">
                <div class="item">
                    <h1 class="display">So close but a world away</h1>
                    <a href="#" class="theme-btn"><span>Call to action</span></a>
                </div>
                <div class="item">
                    <h1 class="display">So close but a world away</h1>
                    <a href="#" class="theme-btn"><span>Call to action</span></a>
                </div>
                <div class="item">
                    <h1 class="display">So close but a world away</h1>
                    <a href="#" class="theme-btn"><span>Call to action</span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <!-- Events filter section -->
        <section class="homepage home-search">

            <div class="home-search-filter">
                <form action="" class="filter home-filter">
                    <ul class="filter">
                        <li>
                            <p>What are you looking for?</p>
                        </li>
                        <li class="select">
                            <select class="form-control">
                                <option>Select Activity</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </li>
                        <li>
                            <input type="text" placeholder="Keywords...">
                        </li>
                        <li>
                            <input data-toggle="datepicker" class="date" placeholder="Date">
                            <label class="date-label">
                                <?php echo file_get_contents("assets/images/icons/calendar.svg"); ?> </label>
                        </li>
                        <li>
                            <button>
                                <i class="fas fa-search"></i> Search</button>
                        </li>
                    </ul>
                </form>
            </div>

            <div class="home-search-content">

                <div class="home-search-header">
                    <h2 class="display">What's On</h2>
                    <ul>
                        <li>
                            <button class="theme-btn bordered">Major events</button>
                        </li>
                        <li>
                            <button class="theme-btn bordered">Today</button>
                        </li>
                        <li>
                            <button class="theme-btn bordered">This weekend</button>
                        </li>
                        <li>
                            <button class="theme-btn bordered">This month</button>
                        </li>
                        <li>
                            <button class="theme-btn bordered">All events</button>
                        </li>
                    </ul>
                </div>

                <div class="home-search-body">
                    <panel class="brown-tag">
                        <a href="#">
                            <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                                <div class="top-tag">Town Name</div>
                                <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                                <div class="desc-overlay">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body">
                                <div class="body-title">
                                    <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Title Of
                                        The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="call-wrapper">
                                        <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                        <a href="tel:02 1234 5678">02 1234 5678</a>
                                    </div>
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </panel>
                    <panel class="brown-tag">
                        <a href="#">
                            <div class="panel-header" style="background-image: url('assets/images/events-2.jpg');">
                                <div class="top-tag">Town Name</div>
                                <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                                <div class="desc-overlay">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body">
                                <div class="body-title">
                                    <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Title Of
                                        The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="call-wrapper">
                                        <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                        <a href="tel:02 1234 5678">02 1234 5678</a>
                                    </div>
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </panel>
                    <panel class="brown-tag">
                        <a href="#">
                            <div class="panel-header" style="background-image: url('assets/images/events-3.jpg');">
                                <div class="top-tag">Town Name</div>
                                <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                                <div class="desc-overlay">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body">
                                <div class="body-title">
                                    <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Title Of
                                        The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="call-wrapper">
                                        <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                        <a href="tel:02 1234 5678">02 1234 5678</a>
                                    </div>
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </panel>
                    <panel class="brown-tag">
                        <a href="#">
                            <div class="panel-header" style="background-image: url('assets/images/events-4.jpg');">
                                <div class="top-tag">Town Name</div>
                                <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                                <div class="desc-overlay">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body">
                                <div class="body-title">
                                    <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Title Of
                                        The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                                </div>
                                <div class="panel-footer">
                                <div class="call-wrapper">
                                    <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                    <a href="tel:02 1234 5678">02 1234 5678</a>
                                </div>
                                <div class="see-more-wrapper">
                                    <a href="#">See More
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                            </div>
                    </panel>
                    <panel class="brown-tag">
                        <a href="#">
                            <div class="panel-header" style="background-image: url('assets/images/events-5.jpg');">
                                <div class="top-tag">Town Name</div>
                                <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                                <div class="desc-overlay">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body">
                                <div class="body-title">
                                    <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Title Of
                                        The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                                </div>
                                <div class="panel-footer">
                                <div class="call-wrapper">
                                    <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                    <a href="tel:02 1234 5678">02 1234 5678</a>
                                </div>
                                <div class="see-more-wrapper">
                                    <a href="#">See More
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                            </div>
                    </panel>
                    <panel class="brown-tag">
                        <a href="#">
                            <div class="panel-header" style="background-image: url('assets/images/events-6.jpg');">
                                <div class="top-tag">Town Name</div>
                                <div class="btm-tag">25 AUG - 27 AUG 2018</div>
                                <div class="desc-overlay">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body">
                            <div class="body-title">
                                <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Title Of
                                    The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                            </div>
                            <div class="panel-footer">
                                <div class="call-wrapper">
                                    <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                    <a href="tel:02 1234 5678">02 1234 5678</a>
                                </div>
                                <div class="see-more-wrapper">
                                    <a href="#">See More
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </panel>
                </div>

                <div class="home-search-footer">
                    <a href="#" class="theme-btn load-more">Load More</a>
                </div>

            </div>
        </section>
        <!-- Events filter section: END -->

        <!-- Newsletter section -->
        <?php include("template-parts/partials/newsletter.php");?>
        <!-- Newsletter section END -->

        <!-- News Section -->
        <section class="homepage home-news">
            <div class="section-header">
                <h2 class="display">News</h2>
                <p>The latest stories from around the region</p>
            </div>
            <div class="home-news-body">
                <div class="home-news-slider-wrapper">
                    <div class="home-news-slider-nav">
                        <button class="news-left"><i class="fas fa-chevron-circle-left"></i></button>
                        <button class="news-right"><i class="fas fa-chevron-circle-right"></i></button>
                    </div>
                    <div class="home-news-slider" id="home-news-slider">
                        <panel class="black-tag">
                            <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                                <div class="top-tag">04 mar 2018</div>
                                <div class="desc-overlay">
                                    <p>Share</p>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="body-cat">
                                    <p>News Categories</p>
                                </div>
                                <div class="body-title">
                                    <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                                </div>
                                <div class="body-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                        ut labore et dolore...</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </panel>
                        <panel class="black-tag">
                            <div class="panel-header" style="background-image: url('assets/images/news-2.jpg');">
                                <div class="top-tag">04 mar 2018</div>
                                <div class="desc-overlay">
                                    <p>Share</p>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="body-cat">
                                    <p>News Categories</p>
                                </div>
                                <div class="body-title">
                                    <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                                </div>
                                <div class="body-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                        ut labore et dolore...</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </panel>
                        <panel class="black-tag">
                            <div class="panel-header" style="background-image: url('assets/images/news-3.jpg');">
                                <div class="top-tag">04 mar 2018</div>
                                <div class="desc-overlay">
                                    <p>Share</p>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="body-cat">
                                    <p>News Categories</p>
                                </div>
                                <div class="body-title">
                                    <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                                </div>
                                <div class="body-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                        ut labore et dolore...</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </panel>
                        <panel class="black-tag">
                            <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                                <div class="top-tag">04 mar 2018</div>
                                <div class="desc-overlay">
                                    <p>Share</p>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="body-cat">
                                    <p>News Categories</p>
                                </div>
                                <div class="body-title">
                                    <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                                </div>
                                <div class="body-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                        ut labore et dolore...</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </panel>
                        <panel class="black-tag">
                            <div class="panel-header" style="background-image: url('assets/images/news-2.jpg');">
                                <div class="top-tag">04 mar 2018</div>
                                <div class="desc-overlay">
                                    <p>Share</p>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="body-cat">
                                    <p>News Categories</p>
                                </div>
                                <div class="body-title">
                                    <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                                </div>
                                <div class="body-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                        ut labore et dolore...</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </panel>
                        <panel class="black-tag">
                            <div class="panel-header" style="background-image: url('assets/images/news-3.jpg');">
                                <div class="top-tag">04 mar 2018</div>
                                <div class="desc-overlay">
                                    <p>Share</p>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="body-cat">
                                    <p>News Categories</p>
                                </div>
                                <div class="body-title">
                                    <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                                </div>
                                <div class="body-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                        ut labore et dolore...</p>
                                </div>
                                <div class="panel-footer">
                                    <div class="see-more-wrapper">
                                        <a href="#">See More
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </panel>
                    </div>
                </div>
                <div class="hot-deals">
                    <panel class="black-tag promo">
                        <div class="panel-header" style="background-image: url('assets/images/news-4.jpg');">
                            <div class="badge">
                                <img src="assets/images/icons/hot-deals.png" alt="hot-deals" clsss="img-fluid">
                            </div>
                        </div>
                        <div class="panel-body bg-theme-primary">
                            <div class="body-cat">
                                <p>Hot Deals</p>
                            </div>
                            <div class="body-promo">
                                <p class="discount">25 Off%</p>
                                <p>SPECIAL DEAL AT THIS LOREM IPSUM DOLOR ADIPISCING ELIT</p>
                            </div>
                            <div class="panel-footer">
                                <div class="see-more-wrapper">
                                    <a href="#">Get Deal
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </panel>
                </div>
            </div>
        </section>
        <!-- News Section: END -->

        <!-- See & Do Section -->
        <section class="homepage home-panel-cta bg-theme-dark">
            <div class="section-header">
                <h2 class="display">See &amp; Do</h2>
            </div>
            <div class="home-panel-cta-body">
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-2.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-3.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
            </div>
            <div class="section-footer">
                <a href="#" class="theme-btn load-more">Load More</a>
            </div>
            <div class="home-panel-cta-links">
                <div class="cta-wrapper" >
                   <div class="row justify-content-center">
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                    <div class="svg-wrapper">
                                    <?php echo file_get_contents("assets/images/icons/attractions.svg"); ?>
                                    </div>
                                    <p>All Attractions</p>
                                </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Guided_Tours.svg"); ?>
                                </div>
                                <p>Guided Tours</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Historical_Walks.svg"); ?>
                                </div>
                                <p>Historical Walks</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Parks_Gardens.svg"); ?>
                                </div>
                                <p>Parks Gardens</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Scenic_Drives.svg"); ?>
                                </div>
                                <p>Scenic Drives</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Fishing_Boating.svg"); ?>
                                </div>
                                <p>Fishing &amp; Boating</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Speciallity_Shopping.svg"); ?>
                                </div>
                                <p>Specialty Shopping</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Fossicking.svg"); ?>
                                </div>
                                <p>Fossicking</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Sporting_Facilities.svg"); ?>
                                </div>
                                <p>Sporting Facilities</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Farm_Experiences.svg"); ?>
                                </div>
                                <p>Farm Experiences</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Markets.svg"); ?>
                                </div>
                                <p>Markets</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Galleries_Museums.svg"); ?>
                                </div>
                                <p>Galleries Museums</p>
                            </a>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- See & Do Section: END -->

        <!-- Eat & Drink -->
        <section class="homepage home-panel-cta bg-theme-light">
            <div class="section-header">
                <h2 class="display">Eat &amp; Drink</h2>
            </div>
            <div class="home-panel-cta-body">
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-2.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-3.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
            </div>
            <div class="section-footer">
                <a href="#" class="theme-btn load-more">Load More</a>
            </div>
            <div class="home-panel-cta-links">
                <div class="cta-wrapper" >
                   <div class="row justify-content-center w-100 mx-auto">
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                    <div class="svg-wrapper">
                                    <?php echo file_get_contents("assets/images/icons/cafe.svg"); ?>
                                    </div>
                                    <p>View All</p>
                                </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Restaurants_Cafes.svg"); ?>
                                </div>
                                <p>Restaurants &amp; Cafes</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/bars.svg"); ?>
                                </div>
                                <p>Bars &amp; Club</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Wineries.svg"); ?>
                                </div>
                                <p>Wineries</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Local_Produce.svg"); ?>
                                </div>
                                <p>Local Produce</p>
                            </a>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Eat & Drink: END -->

        <!-- Stay -->
        <section class="homepage home-panel-cta bg-theme-primary">
            <div class="section-header">
                <h2 class="display">Stay</h2>
            </div>
            <div class="home-panel-cta-body">
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-2.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
                <panel class="brown-tag">
                    <a href="#">
                        <div class="panel-header" style="background-image: url('assets/images/events-3.jpg');">
                            <div class="top-tag">Town Name</div>
                            <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div>
                            <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                            <div class="desc-overlay">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation…</p>
                            </div>
                        </div>
                    </a>
                    <div class="panel-body">
                        <div class="body-title">
                            <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                        </div>
                        <div class="panel-footer">
                            <div class="call-wrapper">
                                <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                                <a href="tel:02 1234 5678">02 1234 5678</a>
                            </div>
                            <div class="see-more-wrapper">
                                <a href="#">See More
                                    <i class="fas fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </panel>
            </div>
            <div class="section-footer">
                <a href="#" class="theme-btn load-more">Load More</a>
            </div>
            <div class="home-panel-cta-links">
                <div class="cta-wrapper" >
                   <div class="row justify-content-center w-100 mx-auto ">
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                    <div class="svg-wrapper">
                                    <?php echo file_get_contents("assets/images/icons/accomodation.svg"); ?>
                                    </div>
                                    <p>All Accomodation</p>
                                </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Cottages.svg"); ?>
                                </div>
                                <p>Cottages</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/bed.svg"); ?>
                                </div>
                                <p>Bed &amp; Breakfast</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Motels_Motor_Inns.svg"); ?>
                                </div>
                                <p>Motels &amp; Motor Inns</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/Hotels.svg"); ?>
                                </div>
                                <p>Hotels</p>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a href="#" class="ctas">
                                <div class="svg-wrapper">
                                <?php echo file_get_contents("assets/images/icons/caravan.svg"); ?>
                                </div>
                                <p>Caraven &amp; Camping</p>
                            </a>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Stay: END -->

        <!-- About Region -->
        <section class="homepage home-collage">
            <div class="section-header">
                <h2 class="display">About the Region</h2>
            </div>
            <div class="collage-body">
                <div class="collage-parent collage-top left">
                    <div class="collage" style="background-color: #768A3A">
                        <a href="#" class="display"><span>History</span></a>
                    </div>
                    <div class="collage w-img" style="background-image: url('assets/images/collage-1.jpg');">
                        <a href="#" class="display"><span>Seasons</span></a>
                    </div>
                    <div class="collage w-img" style="background-image: url('assets/images/collage-2.jpg');">
                        <a href="#" class="display"><span>Tablelands</span></a>
                    </div>
                    <div class="collage w-img" style="background-image: url('assets/images/collage-3.jpg');">
                        <a href="#" class="display"><span>Getting Here</span></a>
                    </div>
                </div>
                <div class="collage-parent collage-top right">
                    <div class="collage w-img w-desc">
                        <!-- <div class="overlay"></div>
                        <a href="#" class="display">
                            <div class="description">
                                <div class="copy-wrapper">
                                <span>The Local's name or locals names</span>
                                <p>Business name for the local or locals</p>
                                </div>
                            </div>
                        </a> -->
                        <!-- <ul class="social-share">
                            <li><a href="#"><i class="fas fa-chevron-circle-left"></i></a></li>
                            <li><a href="#"><i class="fas fa-chevron-circle-right"></i></a></li>
                        </ul> -->
                        <div class="description-slider-img">
                            <a href="#"><div class="description-img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div></a>
                            <a href="#"><div class="description-img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div></a>
                            <a href="#"><div class="description-img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div></a>
                            <a href="#"><div class="description-img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div></a>
                            <a href="#"><div class="description-img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div></a>
                        </div>
                        <div class="description-wrapper">
                            <div class="description-slider">
                                <a href="#" class="description-slider-item">
                                    <div class="description">
                                        <span>The Local's name or locals names 1</span>
                                        <p>Business name for the local or locals</p>
                                    </div>
                                </a>
                                <a href="#" class="description-slider-item">
                                    <div class="description">
                                        <span>The Local's name or locals names</span>
                                        <p>Business name for the local or locals</p>
                                    </div>
                                </a>
                                <a href="#" class="description-slider-item">
                                    <div class="description">
                                        <span>The Local's name or locals names 2</span>
                                        <p>Business name for the local or locals Business name for the local or locals</p>
                                    </div>
                                </a>
                                <a href="#" class="description-slider-item">
                                    <div class="description">
                                        <span>The Local's name or locals names 3</span>
                                        <p>Business name for the local or locals</p>
                                    </div>
                                </a>
                                <a href="#" class="description-slider-item">
                                    <div class="description">
                                        <span>The Local's name or locals names 4</span>
                                        <p>Business name for the local or locals</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="description-slider-nav">
                            <button class="desc-left"><i class="fas fa-chevron-circle-left"></i></button>
                            <button class="desc-right"><i class="fas fa-chevron-circle-right"></i></button>
                        </div>
                    </div>
                    
                </div>
                <div class="collage-parent collage-bottom left">
                    <div class="collage w-img" style="background-image: url('assets/images/collage-4.jpg');">
                        <a href="#" class="display"><span>News</span></a>
                    </div>
                    <div class="collage" style="background-color: #A1855B">
                        <a href="#" class="display"><span>Getting Around</span></a>
                    </div>
                </div>
                <div class="collage-parent collage-bottom right">
                    <div class="collage w-img" style="background-image: url('assets/images/collage-5.jpg');">
                        <a href="#" class="display"><span>Maps</span></a>
                    </div>
                    <div class="collage" style="background-color: #2B2F3D">
                        <a href="#" class="display"><span>Live, Work &amp; Invest</span></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Region: END -->

        <!-- Upper Lachlan & Surrounds -->
        <section class="">
            <div class="section-header">
                <h2 class="display">Upper Lachlan &amp; Surrounds</h2>
            </div>
        </section>
        <!-- Upper Lachlan & Surrounds: END -->
    </div>

</div>

<!-- Accordian map -->
<div class="map-accordian-section bg-theme-light-brown" style="background-image: url('assets/images/svg_map/svg-map-bg.png');">
    <div class="container">
        <div class="row">
            <div id="map-accordian" class="map-accordian">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Upper Lachlan
                        </button>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#map-accordian">
                        <div class="card-body">
                            <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                            <div class="card-copy">
                            <p class="title">Crookwell, Taralga, Gunning</p>
                            <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Goulburn
                        </button>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#map-accordian">
                    <div class="card-body">
                        <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                        <div class="card-copy">
                        <p class="title">Crookwell, Taralga, Gunning</p>
                        <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Hilltops Region
                        </button>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#map-accordian">
                    <div class="card-body">
                        <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                        <div class="card-copy">
                        <p class="title">Crookwell, Taralga, Gunning</p>
                        <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Yass Valley
                        </button>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#map-accordian">
                    <div class="card-body">
                        <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                        <div class="card-copy">
                        <p class="title">Crookwell, Taralga, Gunning</p>
                        <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        Queanbeyan Region
                        </button>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#map-accordian">
                    <div class="card-body">
                        <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                        <div class="card-copy">
                        <p class="title">Crookwell, Taralga, Gunning</p>
                        <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSix">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        Canberra
                        </button>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#map-accordian">
                    <div class="card-body">
                        <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                        <div class="card-copy">
                        <p class="title">Crookwell, Taralga, Gunning</p>
                        <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <div class="svg-map">
                <div class="main-map">
                    <?php echo file_get_contents("assets/images/svg_map/UL_Interactive_Map.svg"); ?> 
                </div>
                <!-- <div id="top-layer">
                    <?php echo file_get_contents("assets/images/svg_map/top-labels.svg"); ?> 
                </div>
                <div id="svg-map-unselected">
                    <div class="hilltops unselected"><?php echo file_get_contents("assets/images/svg_map/hilltops_unselected.svg"); ?> </div>
                    <div class="canberra unselected"><?php echo file_get_contents("assets/images/svg_map/canberra_unselected.svg"); ?> </div>
                    <div class="goulburn unselected"><?php echo file_get_contents("assets/images/svg_map/goulburn_unselected.svg"); ?> </div>
                    <div class="quenbeyan unselected"><?php echo file_get_contents("assets/images/svg_map/queanbeyan_unselected.svg"); ?> </div>
                    <div class="upperlachlan unselected"><?php echo file_get_contents("assets/images/svg_map/upper_lachlan_unselected.svg"); ?> </div>
                    <div class="yass unselected"><?php echo file_get_contents("assets/images/svg_map/yass_unselected.svg"); ?></div> 
                </div>
                <div id="svg-map-hover">
                    <div class="hilltops hover"><?php echo file_get_contents("assets/images/svg_map/hilltops_mouseOver.svg"); ?> </div>
                    <div class="canberra hover"><?php echo file_get_contents("assets/images/svg_map/canberra_mouseOver.svg"); ?> </div>
                    <div class="goulburn hover"><?php echo file_get_contents("assets/images/svg_map/goulburn_mouseOver.svg"); ?> </div>
                    <div class="queanbeyan hover"><?php echo file_get_contents("assets/images/svg_map/queanbeyan_mouseOver.svg"); ?></div> 
                    <div class="upperlachlan hover"><?php echo file_get_contents("assets/images/svg_map/upper_lachlan_mouseOver.svg"); ?> </div>
                    <div class="yass hover"><?php echo file_get_contents("assets/images/svg_map/yass_mouseOver.svg"); ?> </div>
                </div>
                <div id="svg-map-selected">
                    <div class="hilltops selected"><?php echo file_get_contents("assets/images/svg_map/hilltops_selected.svg"); ?> </div>
                    <div class="canberra selected"><?php echo file_get_contents("assets/images/svg_map/canberra_selected.svg"); ?> </div>
                    <div class="goulburn selected"><?php echo file_get_contents("assets/images/svg_map/goulburn_selected.svg"); ?> </div>
                    <div class="queanbeyan selected"><?php echo file_get_contents("assets/images/svg_map/queanbeyan_selected.svg"); ?> </div>
                    <div class="upperlachlan selected"><?php echo file_get_contents("assets/images/svg_map/upper_lachlan_selected.svg"); ?> </div>
                    <div class="yass selected"><?php echo file_get_contents("assets/images/svg_map/yass_selected.svg"); ?> </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Accordian map: END -->

<section class="social">
    <div class="container">
        <div class="section-header">
        <h2 class="display">SOCIAL <span>#visitupperlachlan</span></h2>
        <ul class="links">
            <li><a href="#"><i class="fab fa-youtube-square"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            <li><a href="#"><i class="fab fa-facebook"></i></a></li>
        </ul>
        </div>
    </div>
</section>
<?php include("template-parts/partials/social.php");?>

<?php include("footer.php") ?>
