<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Please feel free to contact us using the form below. One of our Visitor Information Centre staff members will get back to you as soon as possible.</h2>
        </div>
        <div class="inner-page-body">
            <div class="gform_wrapper">
                <div class="gform_heading"></div>
                <div class="gform_body">
                    <ul class="gform_field">
                        <li id="field_1_2" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                            <label class="gfield_label" for="input_1_2">First Name:
                                <span class="gfield_required">*</span>
                            </label>
                            <div class="ginput_container ginput_container_text">
                                <input name="input_2" id="input_1_2" type="text" value="" class="medium" aria-required="true" aria-invalid="false">
                            </div>
                        </li>
                        <li id="field_1_2" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                            <label class="gfield_label" for="input_1_2">Last Name:
                                <span class="gfield_required">*</span>
                            </label>
                            <div class="ginput_container ginput_container_text">
                                <input name="input_2" id="input_1_2" type="text" value="" class="medium" aria-required="true" aria-invalid="false">
                            </div>
                        </li>
                        <li id="field_1_4" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                            <label class="gfield_label" for="input_1_4">Email:
                                <span class="gfield_required">*</span>
                            </label>
                            <div class="ginput_container ginput_container_email">
                                <input name="input_4" id="input_1_4" type="text" value="" class="medium" aria-required="true" aria-invalid="false">
                            </div>
                        </li>
                        <li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                            <label class="gfield_label" for="input_1_3">Phone:
                                <span class="gfield_required">*</span>
                            </label>
                            <div class="ginput_container ginput_container_phone">
                                <input name="input_3" id="input_1_3" type="text" value="" class="medium" aria-required="true" aria-invalid="false">
                            </div>
                        </li>
                        <li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                            <label class="gfield_label" for="input_1_5">Comment
                                <span class="gfield_required">*</span>
                            </label>
                            <div class="ginput_container ginput_container_textarea">
                                <textarea name="input_5" id="input_1_5" class="textarea medium" aria-required="true" aria-invalid="false" rows="10" cols="50"></textarea>
                            </div>
                        </li>
                        <li>
                            <div class="ginput_container_checkbox">
                                <ul class="gfield_checkbox" id="input_2_11">
                                    <li class="gchoice_2_11_7">
                                        <input name="input_11.7" type="checkbox" value="Sponsorship" id="choice_2_11_7">
                                        <label for="choice_2_11_7" id="label_2_11_7">Yes, I would like to receive regular updates</label>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="gform_footer top_label">
                    <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  "
                        onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }">
                    <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
                    <input type="hidden" class="gform_hidden" name="gform_submit" value="1">

                    <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
                    <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjgwZDNlODcwYTVlMDBlMzE0NzFjZjdhNjZhZjJhZmNhIl0=">
                    <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
                    <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
                    <input type="hidden" name="gform_field_values" value="">

                </div>
            </div>
        </div>
    </div>
</section>

<section class="inner-page bg-theme-secondary contact">
    <div class="container">
        <div class="section-header">
            <h2 class="display">Accredited Visitor Information Centres</h2>
        </div>
        <div class="contact-location-wrapper">
            <div class="contact-details">
                <div class="contact-img" style="background-image: url('assets/images/contact-1.png');"></div>
                <div class="contact">
                    <h3>Crookwell Visitor Information Centre</h3>
                    <p>36 Goulburn St <br>Crookwell NSW 2583</p>
                    <p>Ph +612 4832 1988</p>
                    <p>Email - info@visitupperlachlan.com.au</p>
                    <a href="#">Click for directions</a>
                </div>
            </div>
            <div class="contact-map">
                <div id="location-map" class="map-wrapper"></div>
                <div class="contact-hours bg-theme-light">
                    <h5>Opening Hours</h5>
                    <p>Monday - Friday from 9.00am to 5.00pm.</p>
                    <p>Open 10am till 4pm on weekends and public holidays</p>
                </div>
            </div>
        </div>
        <div class="contact-location-wrapper">
            <div class="contact-details">
                <div class="contact-img" style="background-image: url('assets/images/contact-2.png');"></div>
                <div class="contact">
                    <h3>Taralga Gifts &amp; Goodies</h3>
                    <p>23 Orchard St <br>Taralga NSW 2580</p>
                    <p>Ph +612 4840 2558</p>
                    <!-- <p>Email - info@visitupperlachlan.com.au</p> -->
                    <a href="#">Click for directions</a>
                </div>
            </div>
            <div class="contact-map">
                <div id="location-map" class="map-wrapper"></div>
                <div class="contact-hours bg-theme-light">
                    <h5>Opening Hours</h5>
                    <p>Open Thursday to Monday 10am till 4pm</p>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: -34.4574464, lng: 149.4676891};

  var faIcon = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#2B2F3D',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };

  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('location-map'), {zoom: 15, center: uluru});
      
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: faIcon
    });
}
    </script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>
<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>