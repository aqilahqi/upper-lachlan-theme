<section class="newsletter">
    <h2 class="display">Keep up to date</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
    <form action="" class="newsletter">
        <input type="text" placeholder="First Name">
        <input type="text" placeholder="Last Name">
        <input type="email" placeholder="Email Address">
        <button type="submit" class="theme-btn">Subscribe</button>
    </form>
</section>