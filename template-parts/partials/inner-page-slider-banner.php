<header class="inner-page-slider" id="inner-page-slider">
    <div id="img-slider">
        <div class="img-wrapper" style="background-image: url('assets/images/banner/about-meet-locals.jpg');"></div>
        <div class="img-wrapper" style="background-image: url('assets/images/banner/news-banner.png');"></div>
        <div class="img-wrapper" style="background-image: url('assets/images/banner/about-meet-locals.jpg');"></div>
    </div>
    <div class="copy-slider-wrapper">
        <div class="container">
        <div id="copy-slider">
            <div class="copy-wrapper">
                <div class="copy">
                    <h2 class="display">The Local's name or locals names 1</h2>
                    <p>Business name for the local or locals</p>
                </div>
                <a href="#" class="theme-btn">Read More</a>
            </div>
            <div class="copy-wrapper">
                <div class="copy">
                    <h2 class="display">The Local's name or locals names 2</h2>
                    <p>Business name for the local or locals</p>
                </div>
                <a href="#" class="theme-btn">Read More</a>
            </div>
            <div class="copy-wrapper">
                <div class="copy">
                    <h2 class="display">The Local's name or locals names 3</h2>
                    <p>Business name for the local or locals</p>
                </div>
                <a href="#" class="theme-btn">Read More</a>
            </div>
        </div>
        <div class="copy-slider-nav">
            <ul>
                <li><button class="left"><i class="fas fa-chevron-circle-left"></i></button></li>
                <li><button class="right"><i class="fas fa-chevron-circle-right"></i></button></li>
            </ul>
        </div>
        </div>
    </div>
</header>