<section class="social">
    <div class="insta-slider-wrapper">
        <div class="insta-slider-nav">
            <button class="insta-left"><i class="fas fa-chevron-circle-left"></i></button>
            <button class="insta-right"><i class="fas fa-chevron-circle-right"></i></button>
        </div>
        <ul class="instagram-slider" id="instagram-slider">
        <li>
            <a href="#"><img src="assets/images/insta-1.jpg" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-2.jpg" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-3.jpg" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-4.png" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-6.png" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-1.jpg" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-2.jpg" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-3.jpg" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-4.png" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-6.png" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-7.png" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-8.png" alt="" class="img-fluid"></a>
        </li>
        <li>
            <a href="#"><img src="assets/images/insta-9.png" alt="" class="img-fluid"></a>
        </li>
    </ul>
    </div>
</section>