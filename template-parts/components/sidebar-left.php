<div class="sidebar-left-wrapper">
    <div class="thumb-left-marker"><i class="fas fa-ellipsis-v"></i></div>
    <input type="checkbox" class="thumb-left">
    <ul class="sidebar-left">
        <li>
            <div class="svg guide">
                <?php echo file_get_contents("assets/images/icons/Visitor_Guide.svg"); ?> 
            </div>
            <a href="#">
                <div class="text">Guide</div>
            </a>
        </li>
        <li>
            <div class="svg info">
                <?php echo file_get_contents("assets/images/icons/Visitor_Centre_Icon.svg"); ?>
            </div>
            <a href="#">
                <div class="text">Contact</div>
            </a>
        </li>
        <li>
            <div class="svg map">
                <?php echo file_get_contents("assets/images/icons/Maps_Icon.svg"); ?>
            </div>
            <a href="#">
                <div class="text">maps</div>
            </a>
        </li>
        <li>
            <div class="svg share">
                <?php echo file_get_contents("assets/images/icons/Share_Icon.svg"); ?>
            </div>
            <a href="#">
                <i class="fab fa-facebook"></i>
            </a>
            <a href="#">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#">
                <i class="far fa-envelope"></i>
            </a>
        </li>
    </ul>
</div>