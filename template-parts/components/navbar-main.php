<?php
/**
 ** Menu Skeleton
 * 
 * .collapse {
 *   ul.navbar-nav {
 *       li {
 *           &.menu-item-has-children { //if has children
 *               input[type=checkbox] {}
 *               .sub-menu-wrapper {}
 *           }
 *       }
 *   }
 * }
 * 
 * 
 ** Sub Menu Skeleton 
 * 
 * .sub-menu-wrapper {
 *   .container {
 *       .header {
 *           a {}
 *       }
 *       .body {
 *           .body-item {
 *               p.title {}
 *               .content {
 *                   a {
 *                       &.link-w-img {}
 *                       &.link-w-news {}
 *                       &.link-w-links {}
 *                       &.link-w-box {}
 *                   }
 *               }
 *           }
 *       }
 *   }
 * }
 * 
 * 
*/
?>

<nav class="navbar navbar-expand-lg">

    <div class="container">

        <a class="navbar-brand" href="index.php"><img src="assets/images/logos/UL-Header-White.png" alt="Upper Lachlan Logo" class="img-fluid"></a>

        <div class="menu-wrapper">

            <div class="not-collapse">
                <div class="social-icons">
                    <a href="#"><i class="fab fa-youtube-square"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-facebook"></i></a>
                </div>
                <div class="portal">
                    <a href="login.php">
                        <div class="portal-wrapper">
                            <?php echo file_get_contents("assets/images/icons/login.svg"); ?> 
                            <span>Industry Portal</span>
                        </div>
                    </a>
                </div>
                <div class="search">
                    <form action="" class="search-form">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" aria-label="Username" aria-describedby="basic-addon1">
                            <div class="input-group-append">
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu" aria-controls="mainmenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>


            <div class="collapse navbar-collapse" id="mainmenu">
                <ul class="navbar-nav">
                    <li class="menu-item menu-item-has-children">
                        <a class="nav-link" href="#">Explore</a>
                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">
                        <!-- Sub menu -->
                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="about.php">Explore Upper Lachlan <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="seedo_towns_village.php">Towns &amp; Villages </a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="seedo_towns_village_post.php" class="link-w-img">
                                            
                                            <div class="img-slider-wrapper">
                                                <div class="img-slider-nav">
                                                    <button class="img-slider-left"><i class="fas fa-chevron-circle-left"></i></button>
                                                    <button class="img-slider-right"><i class="fas fa-chevron-circle-right"></i></button>
                                                </div>
                                                <div class="nav-img-slider">
                                                <div class="img-wrapper" style="background-image: url('assets/images/post-1.png');"></div>
                                                <div class="img-wrapper" style="background-image: url('assets/images/post-1.png');"></div>
                                                <div class="img-wrapper" style="background-image: url('assets/images/post-1.png');"></div>
                                                <div class="img-wrapper" style="background-image: url('assets/images/post-1.png');"></div>
                                                </div>
                                            </div>
                                            <span>Name of Town</span>
                                            </a>
                                            <!-- <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p> -->
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="explore_scenic_cycling_walking.php">Scenic Drives </a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/bg-home.jpg');"></div>
                                            <!-- <span>Name of Town.</span> -->
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="explore_scenic_cycling_walking.php">Cycling Trails </a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/post-3.png');"></div>
                                            <!-- <span>Name of Town.</span> -->
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="explore_scenic_cycling_walking.php">Walking trails </a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/post-2.png');"></div>
                                            <!-- <span>Name of Town.</span> -->
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Sub menu END -->
                    </li>
                    <li class="menu-item menu-item-has-children"> 
                        <a class="nav-link" href="about.php">About</a>
                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">
                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="about.php">About Upper Lachlan <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="about_meet_the_locals.php">Meet the Locals</a> <i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            
                                            <div class="img-slider-wrapper">
                                                <div class="img-slider-nav">
                                                    <button class="img-slider-left"><i class="fas fa-chevron-circle-left"></i></button>
                                                    <button class="img-slider-right"><i class="fas fa-chevron-circle-right"></i></button>
                                                </div>
                                                <div class="nav-img-slider">
                                                    <div class="img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div>
                                                    <div class="img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div>
                                                    <div class="img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div>
                                                    <div class="img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div>
                                                </div>
                                            </div>
                                            <span>Lorem ipsum dolor sit amet consectetur.</span>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="news.php">Latest News</a> <i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="news_post.php" class="link-w-news">
                                                <h5>March 30th 2018</h5>
                                                <p>Latest news title, Lorem Ipsum lots of great things to see &amp; do...</p>
                                            </a>
                                            <a href="news_post.php" class="link-w-news">
                                                <h5>March 30th 2018</h5>
                                                <p>Latest news title, Lorem Ipsum lots of great things to see &amp; do...</p>
                                            </a>
                                            <a href="news_post.php" class="link-w-news">
                                                <h5>March 30th 2018</h5>
                                                <p>Latest news title, Lorem Ipsum lots of great things to see &amp; do...</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="about.php">About</a> <i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="about_history.php" class="link-w-box">History</a>
                                            <a href="about_history.php" class="link-w-box">Seasons</a>
                                            <a href="about_getting_here.php" class="link-w-box">Getting Here</a>
                                            <a href="about_history.php" class="link-w-box">Getting Around</a>
                                            <a href="about_maps.php" class="link-w-box">Maps</a>
                                            <a href="about_live_work_invest.php" class="link-w-box">Live, Work &amp; Invest</a>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="about_tablelands.php">Tables Region and Canberra</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="about_tablelands.php" class="link-w-box">Upper Lachlan</a>
                                            <a href="about_tablelands.php" class="link-w-box">Goulburn</a>
                                            <a href="about_tablelands.php" class="link-w-box">Hilltops Region</a>
                                            <a href="about_tablelands.php" class="link-w-box">Yass Valley</a>
                                            <a href="about_tablelands.php" class="link-w-box">Queanbeyan Region</a>
                                            <a href="about_tablelands.php" class="link-w-box">Canberra</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-item menu-item-has-children">
                        <a class="nav-link" href="#">Stay</a>
                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">

                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="#">Stay in Upper Lachlan <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="RV_listings.php">RV Friendly Towns</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="RV_listings.php" class="link-w-img">
                                                <div class="img-slider-wrapper">
                                                    <div class="img-slider-nav">
                                                        <button class="img-slider-left"><i class="fas fa-chevron-circle-left"></i></button>
                                                        <button class="img-slider-right"><i class="fas fa-chevron-circle-right"></i></button>
                                                    </div>
                                                    <div class="nav-img-slider">
                                                        <div class="img-wrapper" style="background-image: url('assets/images/post-5.png');"></div>
                                                        <div class="img-wrapper" style="background-image: url('assets/images/post-5.png');"></div>
                                                        <div class="img-wrapper" style="background-image: url('assets/images/post-5.png');"></div>
                                                        <div class="img-wrapper" style="background-image: url('assets/images/post-5.png');"></div>
                                                    </div>
                                                </div>
                                                <span>Lorem ipsum dolor sit amet consectetur.</span>
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item wide">
                                        <p class="title"><a href="seedo_attractions_tours.php">All Accomodation</a> <i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <div class="content-inner">
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Apartments</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Backpackers and Hotels</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Bed and Breakfast</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Caravan, Camping and Holiday Parks</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Cottages</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Farmstay</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Holiday Houses</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Hotels</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Backpackers and Hotels</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Motels</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Resorts</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Retreat and Lodges</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-item menu-item-has-children">
                        <a class="nav-link" href="#">
                            See &amp; do
                        </a>

                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">

                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="about.php">See &amp; Do in Upper Lachlan <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="major_events.php">Major Events</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Crookwell Potato Festival</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Collector Pumpkin Festival</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Crookwell Garden Festival</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Binda Picnic Races</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Gunning Fireworks</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Taralga Rodeo</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Grabine Fishing Tournament</a>
                                            <a href="seedo_attractions_listing.php" class="link-w-box">Crookwell Australia Day</a>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="seedo_attractions_tours.php">All Events</a> <i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Business Event</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Classes &amp; Workshops</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Community Events</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Concert or Performances</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Exhibition and Shows</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Festivals and Celebrations</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Food and Wine</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Markets</a>
                                        </div>
                                    </div>
                                    <div class="body-item wide">
                                        <p class="title"><a href="seedo_attractions_tours.php">All Attractions</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <div class="content-inner">
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Tours</a>
                                                <a href="fossicking.php" class="link-w-box">Fossicking</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Animal and Wildlife</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Agriculture and Industry</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Galleries &amp; Museums</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Historical and Heritage</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">Landmarks and Buildings</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">National Parks and Reserves</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">National Parks and Reserves</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">National Parks and Reserves</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">National Parks and Reserves</a>
                                                <a href="seedo_attractions_listing.php" class="link-w-box">National Parks and Reserves</a>
                                            </div>
                                            <!-- <div class="content-inner">
                                                <a href="#" class="link-w-box">Lorem ipsum dolor sit amet</a>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </li>
                    <li class="menu-item menu-item-has-children">
                        <a class="nav-link" href="#">
                            Eat &amp; drink
                        </a>

                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">

                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="#">Eat &amp; Drink in Upper Lachlan <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="seedo_attractions_tours.php">All Drink &amp; Drink</a></p>
                                        <div class="content">
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Bars</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Breweries</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Cooking Schools &amp; Workshops</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Produce</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Restaurant and Cafés</a>
                                            <a href="seedo_attractions_tours.php" class="link-w-box">Wineries</a>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="seedo_attractions_listing.php">Featured Listings</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/eat-1.png');"></div>
                                            <!-- <span>Lorem ipsum dolor sit amet consectetur.</span> -->
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="seedo_attractions_listing.php">Featured Listings</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/eat-2.png');"></div>
                                            <!-- <span>Lorem ipsum dolor sit amet consectetur.</span> -->
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="seedo_attractions_listing.php">Featured Listings</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/eat-3.png');"></div>
                                            <!-- <span>Lorem ipsum dolor sit amet consectetur.</span> -->
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </li>
                    <li class="menu-item menu-item-has-children">
                        <a class="nav-link" href="#">Groups</a>
                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">
                        <!-- Sub menu -->
                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="groups.php">Groups <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="groups.php">Group Touring Options</a> <i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/group-1.png');"></div>
                                            <span>Lorem ipsum dolor sit amet consectetur.</span>
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="groups.php">Weddings, Parties and meetings</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/group-2.png');"></div>
                                            <span>Lorem ipsum dolor sit amet consectetur.</span>
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="groups_suggested_itineraries.php">Suggested Itineraries</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/group-3.png');"></div>
                                            <span>Lorem ipsum dolor sit amet consectetur.</span>
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Sub menu END -->
                    </li>
                    <li class="menu-item menu-item-has-children">
                        <a class="nav-link" href="contact.php">
                            Contact us
                        </a>

                        <input type="checkbox" class="dropdown-checkbox" name="dropdown-checkbox">

                        <div class="sub-menu-wrapper">
                            <div class="container">
                                <div class="header">
                                    <a href="#">Contact Us and Further Information  <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                                <div class="body">
                                    <div class="body-item">
                                        <p class="title"><a href="#">Contact Us</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-box">Submit an Enquiry</a>
                                            <a href="#" class="link-w-box">Visitor Information Centres</a>
                                            <a href="#" class="link-w-box">Privacy Policy</a>
                                            <a href="#" class="link-w-box"><i class="fas fa-sign-in-alt"></i> Industry Portal</a>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="#">Visitor Guide</a> <a href="#" class="download"><i class="fas fa-download"></i> Download PDF</a></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <!-- <div class="img-wrapper" style="background-image: url('assets/images/download.png');"></div> -->
                                            <img src="assets/images/download.png" alt="download" class="img-fluid">
                                            <!-- <span>Lorem ipsum dolor sit amet consectetur.</span> -->
                                            </a>
                                            <!-- <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p> -->
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="competition_page.php">Current Competition Page</a><i class="fas fa-chevron-right"></i></p>
                                        <div class="content">
                                            <a href="#" class="link-w-img">
                                            <div class="img-wrapper" style="background-image: url('assets/images/menu-post-1.jpg');"></div>
                                            <span>Lorem ipsum dolor sit amet consectetur.</span>
                                            </a>
                                            <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, obcaecati.</p>
                                        </div>
                                    </div>
                                    <div class="body-item">
                                        <p class="title"><a href="#">Useful Links</a></p>
                                        <div class="content">
                                            <a href="#" class="link-w-links">
                                                <h5>Link Title, lorem ipsum</h5>
                                                <p>Website Link</p>
                                            </a>
                                            <a href="#" class="link-w-links">
                                                <h5>Link Title, lorem ipsum</h5>
                                                <p>Website Link</p>
                                            </a>
                                            <a href="#" class="link-w-links">
                                                <h5>Link Title, lorem ipsum</h5>
                                                <p>Website Link</p>
                                            </a>
                                            <a href="#" class="link-w-links">
                                                <h5>Link Title, lorem ipsum</h5>
                                                <p>Website Link</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </li>
                </ul>
            </div>

        </div>

    </div>

</nav>