<div class="sidebar-right-wrapper">
    <ul class="sidebar-right">
        <li class="sidebar-item">
            <div class="thumb-right-marker"><i class="fas fa-ellipsis-v"></i></div>
            <input type="checkbox" class="thumb-right">
            <div class="sidebar-right-children">
                <ul>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                    <li><a href="#">Anchored Menu Item</a></li>
                </ul>
            </div>
        </li>
    </ul>
</div>