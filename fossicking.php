<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div>
        <div class="inner-page-body">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <img src="assets/images/about-history-post.png" alt="about-history" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="display">Lorem ipsum dolor sit amet</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inner-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="section-header">
                <h2 class="display black">Fossicking Locations</h2>
            </div>
        </div>
    </div>
    <div class="explore-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-xs-12 order-md-1">
                    <div id="location-map" class="explore-map"></div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="explore-panel-wrapper">
                        <panel class="fossicking">
                            <a href="#" class="panel-header-link">
                                <div class="panel-header" style="background-image: url('assets/images/fossicking-1.png');"></div>
                            </a>
                            <div class="panel-body bg-theme-light-brown">
                                <h3 class="display">Name of fossicking location</h3>
                            </div>
                            <div class="panel-footer">
                                <a href="#">Click for Directions <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="fossicking">
                            <a href="#" class="panel-header-link">
                                <div class="panel-header" style="background-image: url('assets/images/fossicking-1.png');"></div>
                            </a>
                            <div class="panel-body bg-theme-light-brown">
                                <h3 class="display">Name of fossicking location</h3>
                            </div>
                            <div class="panel-footer">
                                <a href="#">Click for Directions <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <!-- <panel class="fossicking">
                            <a href="#" class="panel-header-link">
                                <div class="panel-header" style="background-image: url('assets/images/fossicking-1.png');"></div>
                            </a>
                            <div class="panel-body bg-theme-light-brown">
                                <h3 class="display">Name of fossicking location</h3>
                            </div>
                            <div class="panel-footer">
                                <a href="#">Click for Directions <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="fossicking">
                            <a href="#" class="panel-header-link">
                                <div class="panel-header" style="background-image: url('assets/images/fossicking-1.png');"></div>
                            </a>
                            <div class="panel-body bg-theme-light-brown">
                                <h3 class="display">Name of fossicking location</h3>
                            </div>
                            <div class="panel-footer">
                                <a href="#">Click for Directions <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->

<script>
    // // Initialize and add the map
    // function initMap() {
    //     // The location of Uluru
    //     var uluru = {
    //         lat: -34.3461707,
    //         lng: 149.5000172
    //     };
    //     // The map, centered at Uluru
    //     var map = new google.maps.Map(
    //         document.getElementById('location-map'), {
    //             zoom: 8,
    //             center: uluru
    //         });
    //     // The marker, positioned at Uluru
    //     var marker = new google.maps.Marker({
    //         position: uluru,
    //         map: map
    //     });
    // }
    function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("location-map"), mapOptions);
    // map.setTilt(45);
    var faIcon = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#2B2F3D',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    var faIcon_click = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#A1855B',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    // Multiple Markers
    var markers = [
        ['Laggan, NSW', -34.3461707, 149.5000172],
        ['Grassmere Park, NSW', -34.7446904,149.688714]
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<panel class="fossicking map"><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
        
        ['<panel class="fossicking map"><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: faIcon,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                // infowindow.close();
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
                var gm = jQuery('.gm-style-iw').parent();
                var gm_ = jQuery('.gm-style-iw').prev();
                var gm_close = jQuery('.gm-style-iw').next();
                var gm_arrow = gm_.children().eq(2);
                var gm_white = gm_.children().last();
                var gm_shadow = gm_arrow.prev();
                var gm_arrow_shadow = gm_shadow.prev();
                gm.addClass('parent-info-window');
                gm_.addClass('wrapper-info-window');
                gm_arrow.addClass('wrapper-arrow');
                gm_white.addClass('wrapper-white');
                gm_shadow.addClass('wrapper-shadow');
                gm_arrow_shadow.addClass('arrow-shadow');
                gm_close.addClass('close-window');
            }
        })(marker, i));

        // infowindow.open(map);
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon_click); 
            }
        })(marker, i));

        // on mouseout (moved mouse off marker) make infoWindow disappear
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon); 
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });

}


</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>

<?php include("footer.php") ?>