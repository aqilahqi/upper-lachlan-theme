<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page default">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">This following document sets forth the Privacy Policy for the Upper Lachlan Shire Tourist Association website, www.visitupperlachlan.com.au</h2>
        </div>
        <div class="inner-page-body">
            <p>The Upper Lachlan Shire Tourist Association is committed to providing you with the best possible customer service experience. The Upper Lachlan Shire Tourist Association is bound by the Privacy Act 1988, which sets out a number of principles concerning the privacy of individuals.</p>
            <br>
            <h5>Collection of your personal information</h5>
            <br>
            <p>There are many aspects of the site which can be viewed without providing personal information, however, for access to future Upper Lachlan Shire Tourist Association customer support features you may be required to submit personally identifiable information. This may include but not limited to a unique username and password, or provide sensitive information in the recovery of your lost password.</p>
            <br>
            <h5>Sharing of your personal information</h5>
            <br>
            <p>We may occasionally hire other companies to provide services on our behalf, including but not limited to handling customer support enquiries, processing transactions or customer freight shipping. Those companies will be permitted to obtain only the personal information they need to deliver the service. The Upper Lachlan Shire Tourist Association takes reasonable steps to ensure that these organisations are bound by confidentiality and privacy obligations in relation to the protection of your personal information.</p>
            <br>
            <h5>Use of your personal information</h5>
            <br>
            <p>For each visitor to reach the site, we expressively collect the following non-personally identifiable information, including but not limited to browser type, version and language, operating system, pages viewed while browsing the Site, page access times and referring website address. This collected information is used solely internally for the purpose of gauging visitor traffic, trends and delivering personalized content to you while you are at this Site.</p>
            <br>
            <p>From time to time, we may use customer information for new, unanticipated uses not previously disclosed in our privacy notice. If our information practices change at some time in the future we will use for these new purposes only, data collected from the time of the policy change forward will adhere to our updated practices.</p>
            <br>
            <h5>Changes to this Privacy Policy</h5>
            <br>
            <p>The Upper Lachlan Shire Tourist Association reserves the right to make amendments to this Privacy Policy at any time. If you have objections to the Privacy Policy, you should not access or use the Site.</p>
            <br>
            <h5>Accessing Your Personal Information</h5>
            <br>
            <p>You have a right to access your personal information, subject to exceptions allowed by law. If you would like to do so, please let us know. You may be required to put your request in writing for security reasons. The Upper Lachlan Shire Tourist Association reserves the right to charge a fee for searching for, and providing access to, your information on a per request basis.</p>
            <br>
            <h5>Contacting Us</h5>
            <br>
            <p>The Upper Lachlan Shire Tourist Association welcomes your comments regarding this Privacy Policy. If you have any questions about this Privacy Policy and would like further information, please contact us by any of the following means during business hours Monday to Friday.</p>
            <br>
            <h5>Call: (02) 4832 1988</h5>
            <br>
            <h5>Post: Attn: Privacy Policy,
<br>
Upper Lachlan Shire Tourist Association
<br>
106 Goulburn St, Crookwell, NSW, 2583
<br>
AUSTRALIA</h5>
<br>
<h5>E-mail: crookwellvisitor@bigpond.com</h5>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>