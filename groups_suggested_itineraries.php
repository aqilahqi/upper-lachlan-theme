<?php include("header.php") ?>
<?php include("template-parts/partials/inner-page-slider-banner.php");?>

<!-- Breadcrumb -->
<nav class="breadcrumb border-b">
    <div class="container">
        <ul>
            <li><a href="/">Home</a></li>
            <li class="active">Breadcrumb</li>
        </ul>
    </div>
</nav>
<!-- Breadcrumb: END -->

<section class="news groups-itinerary">
    <div class="container">
        <div class="section-header">
            <h1 class="display dark">Suggested Itineraries</h1>
            <ul class="filter">
                <li><button class="theme-btn bordered">1/2 Day</button></li>
                <li><button class="theme-btn bordered">1 Day</button></li>
                <li><button class="theme-btn bordered">2 Day</button></li>
            </ul>
        </div>
        <div class="panel-wrapper d-md-flex">
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="black-tag">
                <div class="panel-header" style="background-image: url('assets/images/news-1.jpg');">
                    <div class="top-tag">Length in Days</div>
                    <div class="desc-overlay">
                        <p>Share</p>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="body-cat">
                        <p>News Categories</p>
                    </div>
                    <div class="body-title">
                        <p>The best thing about lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit</p>
                    </div>
                    <div class="body-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore...</p>
                    </div>
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="groups_siggested_itineraries_post.php">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
        </div>
        <div class="section-footer">
            <div class="row justify-content-center">
                <a href="#" class="theme-btn">load more</a>
            </div>
        </div>
    </div>
</section>

<?php include("template-parts/partials/newsletter.php");?>
<?php include("footer.php") ?>