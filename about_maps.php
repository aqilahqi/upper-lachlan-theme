<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light maps-section">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">To view info on all our towns and villages <a href="#">click here</a>. Or download detailed Maps in PDF format below.</h2>
        </div>
        <div class="inner-page-body maps">
            <div class="row">
                <div class="col-xs-12 col-md-6 maps-content-left" style="background-image: url('assets/images/about-maps.jpg');">
                    <div class="capital-wrapper">
                        <h5 class="display">Capital Country Region</h5>
                        <a href="#" class="theme-btn brown"><i class="far fa-file-pdf"></i> <span>Download PDF</span></a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 maps-content-right">
                    <div class="township-wrapper bg-theme-primary">
                        <h5 class="display">Crookwell Township</h5>
                        <a href="#" class="theme-btn brown"><i class="far fa-file-pdf"></i> <span>Download PDF</span></a>
                    </div>
                    <div class="township-wrapper bg-theme-secondary">
                        <h5 class="display">Gunning Township</h5>
                        <a href="#" class="theme-btn brown"><i class="far fa-file-pdf"></i> <span>Download PDF</span></a>
                    </div>
                    <div class="township-wrapper bg-theme-tertiary">
                        <h5 class="display">Taralga Township</h5>
                        <a href="#" class="theme-btn brown"><i class="far fa-file-pdf"></i> <span>Download PDF</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Featured Listing -->
<?php include("template-parts/partials/featured-listings.php");?>
<!-- Featured Listing: END -->

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>