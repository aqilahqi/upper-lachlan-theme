<footer class="footer-wrapper">
    <div class="container">
        <div class="footer-lv footer-lv-one">
            <div class="column-link">
                <p>SEE &amp; DO</p>
                <ul>
                    <li><a href="#">All Attractions</a></li>
                    <li><a href="#">All Events</a></li>
                    <li><a href="seedo_towns_villages">Towns &amp; Villages</a></li>
                    <li><a href="#">Explore</a></li>
                    <li><a href="#">Fossicking</a></li>
                    <li><a href="#">Tours</a></li>
                    <li><a href="#">Suggested Itineraries</a></li>
                    <li><a href="#">Groups</a></li>
                    <li><a href="#">Weddings, Parties and Meetings</a></li>
                </ul>
            </div>
            <div class="column-link">
                <p>Major Events</p>
                <ul>
                    <li><a href="#">Crookwell Potato Festival</a></li>
                    <li><a href="#">Collector Pumpkin Festival</a></li>
                    <li><a href="#">Crookwell Garden Festival</a></li>
                    <li><a href="#">Binda Picnic Races</a></li>
                    <li><a href="#">Gunning Fireworks</a></li>
                    <li><a href="#">Taralga Rodeo</a></li>
                    <li><a href="#">Grabine Fishing Tournament</a></li>
                </ul>
            </div>
            <div class="column-link">
                <p>Stay</p>
                <ul>
                    <li><a href="#">All Accomodation</a></li>
                </ul>
                <p>Eat &amp; Drink</p>
                <ul>
                    <li><a href="#">All Eat &amp; Drink</a></li>
                </ul>
                <p>Information</p>
                <ul>
                    <li><a href="#">Visitor Information Centres</a></li>
                    <li><a href="#">Download Visitor Guide</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
            <div class="column-link">
                <p>About</p>
                <ul>
                    <li><a href="about_history.php">History</a></li>
                    <li><a href="about_meet_the_locals.php">Meet the locals</a></li>
                    <li><a href="news.php">News</a></li>
                    <li><a href="about_history.php">Seasons</a></li>
                    <li><a href="about_getting_here.php">Getting Here</a></li>
                    <li><a href="about_history.php">Getting Around</a></li>
                    <li><a href="about_tablelands.php">Tablelands</a></li>
                    <li><a href="about_maps.php">Maps</a></li>
                    <li><a href="about_live_work_invest.php">Live, Work &amp; Invest</a></li>
                </ul>
            </div>
        </div>
        <div class="footer-lv footer-lv-two">
            <div class="social-icons">
                <p>social</p>
                <a href="#"><i class="fab fa-youtube-square"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-facebook"></i></a>
            </div>
            <div class="logo-wrapper">
                <div class="cbr-logo">
                    <img src="assets/images/logos/cbr_logo.png" alt="cbr_logo" class="img-fluid">
                    <img src="assets/images/logos/cbr_tl_logo.png" alt="cbr_tl_logo" class="img-fluid">
                </div>
                <img src="assets/images/logos/tripadvisor_white_logo.png" alt="tripadvisor_logo" class="tripadvisor img-fluid">
                <img src="assets/images/logos/ul_shire_logo.png" alt="ul_shire" class="shire img-fluid">
            </div>
            <div class="portal">
                <a href="login.php">
                    <?php echo file_get_contents("assets/images/icons/login.svg"); ?> 
                    <span>Industry Portal</span>
                </a>
            </div>
        </div>
        <div class="footer-lv footer-lv-three">
            <div class="footer-lv-three-links">
                <ul>
                    <li>&copy;2018 Upper Lachlan Tourist Association</li>
                    <li><a href="default_template.php">Privacy Policy</a></li>
                    <li><a href="contact.php">Contact Details</a></li>
                </ul>
                <p>Website by <a href="https://www.oskyinteractive.com.au/" target="_blank">Osky</a></p>
            </div>
            <div class="footer-lv-three-disclaimer">
                <p>This service is provided by the Upper Lachlan Tourist Association (ULTA). The ULTA provides this information with the understanding that it is not guaranteed to be accurate, correct or complete. Conclusions drawn from this information are the responsibility of the user. Every effort has been made to ensure accuracy however, the ULTA assumes no responsibility in the event that any information is incorrect. The ULTA assumes no liability for damages incurred as a result of incomplete, incorrect or omitted information. Businesses/services included in this directory are not necessarily endorsed or approved by the ULTA. The user of this information assumes all liability for their dependence on it.</p>
            </div>
        </div>
    </div>
</footer>
<script src="assets/js/main.js"></script>

<script src="assets/js/datepicker.min.js"></script>
</body>
</html>