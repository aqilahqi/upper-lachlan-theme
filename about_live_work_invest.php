<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div>
        <div class="inner-page-body">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <img src="assets/images/about-live-post.png" alt="about-history" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="display">Lorem ipsum dolor sit amet</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inner-page">
    <div class="container">
        <div class="section-header">
            <h2 class="display black">So Much to Offer</h2>
        </div>
        <div class="row">
            <div class="col-md-8 col-xs-12 embed-left">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                </div>
                <h5 class="display">Collector Wines Canberra Region</h5>
            </div>
            <div class="col-md-4 col-xs-12 embed-right">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"></iframe>
                </div>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"></iframe>
                </div>
                <a href="#" target="_blank" class="more-videos"><i class="fab fa-youtube-square"></i> View more videos on YouTube</a>
            </div>
        </div>
    </div>
</section>

<section class="inner-page facts bg-theme-secondary">
    <div class="container">
        <div class="section-header">
            <h2 class="display">Facts &amp; Stats</h2>
            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua 2016 census. All facts are from home.id.com.au <br> Please visit <a href="https://profile.id.com.au/upper-lachlan">https://profile.id.com.au/upper-lachlan</a> for detailed information.</p>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <?php echo file_get_contents("assets/images/icons/Population.svg"); ?> 
                <p>Population</p>
                <h5>7,841</h5>
            </div>
            <div class="col-lg-3 col-md-6">
                <?php echo file_get_contents("assets/images/icons/Median_Age.svg"); ?> 
                <p>Median Age</p>
                <h5>48</h5>
            </div>
            <div class="col-lg-3 col-md-6">
                <?php echo file_get_contents("assets/images/icons/Rental.svg"); ?> 
                <p>Median Weekly Rent</p>
                <h5>$193</h5>
            </div>
            <div class="col-lg-3 col-md-6">
                <?php echo file_get_contents("assets/images/icons/Land_Area.svg"); ?> 
                <p>Land Area</p>
                <h5>712,883ha</h5>
            </div>
        </div>
    </div>
</section>

<section class="featured-listing">
    <div class="container">
        <div class="section-header">
            <h2 class="display">Real Estate</h2>
        </div>
        <div class="panel-wrapper d-md-flex justify-content-between">
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <!-- <div class="btm-tag">Price range</div> -->
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <!-- <div class="btm-tag">25 AUG - 27 AUG 2018</div> -->
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="brown-tag">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/events-1.jpg');">
                        <div class="top-tag">Town Name</div>
                        <!-- <div class="btm-left-tag"><img src="assets/images/logos/tripadvisor.png" alt="tripadvisor" class="img-fluid"></div> -->
                        <!-- <div class="btm-tag">Price range</div> -->
                        <div class="desc-overlay">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, molestias ratione odio officiis vero
                                voluptatum illum. Sit eum iste ab rerum, cupiditate facilis mollitia quisquam a, iure perspiciatis
                                aliquid odit?</p>
                        </div>
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <p>Title Of The Listing Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="call-wrapper">
                            <?php echo file_get_contents("assets/images/icons/phone_gold.svg"); ?>
                            <a href="tel:02 1234 5678">02 1234 5678</a>
                        </div>
                        <div class="see-more-wrapper">
                            <a href="#">See More
                                <i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>