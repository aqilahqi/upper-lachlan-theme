<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page">
    <div class="explore-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="route-wrapper">
                    <div class="route-header">
                        <ul>
                            <li><p>View More scenic drives</p></li>
                            <li><button class="left-drives"><i class="fas fa-chevron-circle-left"></i></button></li>
                            <li><button class="right-drives"><i class="fas fa-chevron-circle-right"></i></button></li>
                        </ul>
                    </div>
                    <div class="route-body" id="route-body">
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                        <panel class="route">
                            <div class="panel-header bg-theme-primary">
                                <h3>ROUTE No.</h3>
                                <p>Description lorem ipsum dolor sit amet, consectetur adipiscing</p>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li class="time"><p><b>Driving Time:</b> 1hr 45mins</p></li>
                                    <li class="speed"><p><b>Average Speed:</b> 40 - 50 kmh</p></li>
                                    <li class="distance"><p><b>Distance:</b> 57km</p></li>
                                    <li class="notes"><p><b>Notes:</b> We suggest a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></li>
                                </ul>
                            </div>
                            <div class="panel-footer bg-theme-primary">
                                <a href="#"> <i class="fas fa-download"></i>Download</a>
                                <a href="#"> View Route <i class="fas fa-chevron-circle-right"></i></a>
                            </div>
                        </panel>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="section-header">
                    <a href="#" class="caret"><i class="fas fa-caret-down"></i></a>
                    <h3 class="display">Small outline of the route and overview of area covered, lorem ipsum dolor sit amet.</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-xs-12 order-md-1">
                    <div id="location-map" class="explore-map"></div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div class="explore-panel-wrapper">
                        <panel class="explore bg-theme-light" data-lat="-33.8603222" data-lng="148.658563">
                            <div class="panel-header">
                                <i class="fas fa-map-marker-alt"></i>
                                <h3 class="display">Location of milestone on the route</h3>
                            </div>
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.</p>
                            </div>
                            <div class="panel-footer d-flex justify-content-end">
                                <a href="#" class="theme-btn">Link to Other Content</a>
                            </div>
                        </panel>
                        <panel class="explore bg-theme-light" data-lat="-33.936573" data-lng="148.596399">
                            <div class="panel-header">
                                <i class="fas fa-map-marker-alt"></i>
                                <h3 class="display">Location of milestone on the route</h3>
                            </div>
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.</p>
                            </div>
                            <div class="panel-footer d-flex justify-content-end">
                                <a href="#" class="theme-btn">Link to Other Content</a>
                            </div>
                        </panel>
                        <panel class="explore bg-theme-light" data-lat="-34.637652" data-lng="148.025006">
                            <div class="panel-header">
                                <i class="fas fa-map-marker-alt"></i>
                                <h3 class="display">Location of milestone on the route</h3>
                            </div>
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.</p>
                            </div>
                            <div class="panel-footer d-flex justify-content-end">
                                <a href="#" class="theme-btn">Link to Other Content</a>
                            </div>
                        </panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

</script>


<!-- Featured Listing -->
<?php include("template-parts/partials/featured-listings.php");?>
<!-- Featured Listing: END -->

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->

<?php include("footer.php") ?>
<script src="assets/js/trails-map.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>
