<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page bg-theme-light">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div>
        <div class="inner-page-body">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <img src="assets/images/about-tablelands-post.jpg" alt="about-history" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="display">Lorem ipsum dolor sit amet</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inner-page bg-theme-primary">
    <div class="container">
        <div class="section-header">
            <h2 class="display">Tablelands Region &amp; Canberra</h2>
        </div>
        <div class="tableland-map-wrapper">
            <!-- Accordian map -->
        <div class="map-accordian-section">
        <div id="map-accordian" class="map-accordian">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Upper Lachlan
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#map-accordian">
                            <div class="card-body">
                                <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                                <div class="card-copy">
                                <p class="title">Crookwell, Taralga, Gunning</p>
                                <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                                </div>
                                <div class="card-footer">
                                    <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Collapsible Group Item #2
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#map-accordian">
                        <div class="card-body">
                            <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                            <div class="card-copy">
                            <p class="title">Crookwell, Taralga, Gunning</p>
                            <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Collapsible Group Item #3
                            </button>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#map-accordian">
                        <div class="card-body">
                            <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                            <div class="card-copy">
                            <p class="title">Crookwell, Taralga, Gunning</p>
                            <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Collapsible Group Item #4
                            </button>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#map-accordian">
                        <div class="card-body">
                            <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                            <div class="card-copy">
                            <p class="title">Crookwell, Taralga, Gunning</p>
                            <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Collapsible Group Item #5
                            </button>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#map-accordian">
                        <div class="card-body">
                            <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                            <div class="card-copy">
                            <p class="title">Crookwell, Taralga, Gunning</p>
                            <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Collapsible Group Item #6
                            </button>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#map-accordian">
                        <div class="card-body">
                            <div class="img-wrapper" style="background-image: url('assets/images/collage-6.jpg');"></div>
                            <div class="card-copy">
                            <p class="title">Crookwell, Taralga, Gunning</p>
                            <p>Short description of the region, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="theme-btn"><span>View Our Villages</span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- Accordian map: END -->
        <div id="location-map" class="tableland-map"></div>
        </div>
    </div>
</section>

<!-- Featured Listing -->
<?php include("template-parts/partials/featured-listings.php");?>
<!-- Featured Listing: END -->

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->

<script>
    function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("location-map"), mapOptions);
    // map.setTilt(45);
    var faIcon = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#2B2F3D',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    var faIcon_click = {
        path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
        fillColor: '#A1855B',
        fillOpacity: 1,
        scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
    };
    // Multiple Markers
    var markers = [
        ['Laggan, NSW', -34.3461707, 149.5000172],
        ['Grassmere Park, NSW', -34.7446904,149.688714]
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<panel class="plain map"><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
        
        ['<panel class="plain map"><div class="panel-body"><div class="body-title"><h3 class="display">Name of the town or village</h3></div><div class="panel-footer"><div class="see-more-wrapper"><a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i></a></div></div></div></panel>'],
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: faIcon,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                // infowindow.close();
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
                var gm = jQuery('.gm-style-iw').parent();
                var gm_ = jQuery('.gm-style-iw').prev();
                var gm_close = jQuery('.gm-style-iw').next();
                var gm_arrow = gm_.children().eq(2);
                var gm_white = gm_.children().last();
                var gm_shadow = gm_arrow.prev();
                var gm_arrow_shadow = gm_shadow.prev();
                gm.addClass('parent-info-window');
                gm_.addClass('wrapper-info-window');
                gm_arrow.addClass('wrapper-arrow');
                gm_white.addClass('wrapper-white');
                gm_shadow.addClass('wrapper-shadow');
                gm_arrow_shadow.addClass('arrow-shadow');
                gm_close.addClass('close-window');
                // for (var i = 0; i < infoWindowContent.length; i++ ) {  //I assume you have your infoboxes in some array
                //     infoWindowContent[i][0].close();
                // }
            }
        })(marker, i));

        // infowindow.open(map);
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon_click); 
            }
        })(marker, i));

        // on mouseout (moved mouse off marker) make infoWindow disappear
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
            return function() {
                marker.setIcon(faIcon); 
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });

}


</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>
<?php include("footer.php") ?>