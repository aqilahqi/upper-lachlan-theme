<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>


<section class="featured-listing major-events ">
    <div class="container">
        <div class="section-header rv">
            <h2 class="display mx-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div>
        <div class="panel-wrapper rv d-md-flex">
            <panel class="plain">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                        <img src="assets/images/logos/rv.jpg" alt="">
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <h3 class="display">Name of the town or village</h3>
                    </div>
                    <!-- <div class="body-copy">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                    </div> -->
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="plain">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                    <img src="assets/images/logos/rv.jpg" alt="">
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <h3 class="display">Name of the town or village</h3>
                    </div>
                    <!-- <div class="body-copy">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                    </div> -->
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
            <panel class="plain">
                <a href="#">
                    <div class="panel-header" style="background-image: url('assets/images/post-1.png');">
                    <img src="assets/images/logos/rv.jpg" alt="">
                    </div>
                </a>
                <div class="panel-body">
                    <div class="body-title">
                        <h3 class="display">Name of the town or village</h3>
                    </div>
                    <!-- <div class="body-copy">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa.</p>
                    </div> -->
                    <div class="panel-footer">
                        <div class="see-more-wrapper">
                            <a href="#">SEE MORE<i class="fas fa-chevron-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </panel>
        </div>
    </div>
</section>

<section class="faq">
    <div class="container">
        <div class="section-header">
            <h2 class="display">FAQs and Information</h2>
        </div>
        <div id="accordion" class="faq-accordian">
            <div class="card">
                <div class="card-header" id="heading1">
                    <h5 class="display black">
                    <button class="text-link" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                    This is a title and some info for an accordion item - Lorem ipsum dolor sit amet, consectetur adipiscing am.
                    </button>
                    </h5>
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                        <span></span>
                        <span></span>
                    </button>
                </div>

                <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordion">
                <div class="card-body bg-theme-light">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <b>Ut enim ad minim veniam.</b> Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <ul>
                        <li> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</li>
                        <li> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading2">
                    <h5 class="display black">
                    <button class="text-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    This is a title and some info for an accordion item - Lorem ipsum dolor sit amet, consectetur adipiscing am.
                    </button>
                    </h5>
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                <div class="card-body bg-theme-light">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <b>Ut enim ad minim veniam.</b> Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <ul>
                        <li> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</li>
                        <li> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading3">
                <h5 class="display black">
                    <button class="text-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                    This is a title and some info for an accordion item - Lorem ipsum dolor sit amet, consectetur adipiscing am.
                    </button>
                    </h5>
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                <div class="card-body bg-theme-light">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <b>Ut enim ad minim veniam.</b> Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <ul>
                        <li> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</li>
                        <li> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</li>
                    </ul>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("template-parts/partials/featured-listings.php");?>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>