<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page portal-resources">
    <div class="container">
        <div class="filter-area">
            <ul>
                <li><button class="theme-btn bordered">Filter name for items</button></li>
                <li><button class="theme-btn bordered">Filter name for items</button></li>
                <li>
                    <div class="form-check">
                        <select class="form-control">
                            <option>Sort By name (A-Z)</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </li>
                <li>
                    <input type="text" placeholder="Add Keywords...">
                </li>
            </ul>
        </div>
        <div class="resources-wrapper">
            <div class="resources-item">
                <div class="resource-details">
                    <div class="detail-body">
                        <h3 class="display"><i class="far fa-file-pdf"></i>Document Title</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt, Vide referrentur dissentiunt per ne...</p>
                    </div>
                    <div class="detail-footer">
                    <a href="#"><i class="fas fa-download"></i> Download PDF <span>356kb</span></a>
                    </div>
                </div>
                <div class="resource-cover">
                    <img src="assets/images/pdf-1.png" alt="">
                </div>
            </div>
            <div class="resources-item">
                <div class="resource-details">
                    <div class="detail-body">
                        <h3 class="display"><i class="far fa-file-pdf"></i>Document Title</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt, Vide referrentur dissentiunt per ne...</p>
                    </div>
                    <div class="detail-footer">
                    <a href="#"><i class="fas fa-download"></i> Download PDF <span>356kb</span></a>
                    </div>
                </div>
                <div class="resource-cover">
                    <img src="assets/images/pdf-1.png" alt="">
                </div>
            </div>
            <div class="resources-item">
                <div class="resource-details">
                    <div class="detail-body">
                        <h3 class="display"><i class="far fa-file-pdf"></i>Document Title</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt, Vide referrentur dissentiunt per ne...</p>
                    </div>
                    <div class="detail-footer">
                    <a href="#"><i class="fas fa-download"></i> Download PDF <span>356kb</span></a>
                    </div>
                </div>
                <div class="resource-cover">
                    <img src="assets/images/pdf-1.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>