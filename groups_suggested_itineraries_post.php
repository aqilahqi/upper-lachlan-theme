<?php include("header.php") ?>

<section class="news news-post itinerary">
    <div class="container">
        <div class="post-header">
            <ul>
                <li><a href="#" class="theme-btn">Download Itinerary</a></li>
                <li>
                    <div class="navigation">
                        <ul>
                            <li><a href="#" class="left-nav"> Previous Itinerary</a></li>
                            <li><a href="#" class="right-nav"> Next Itinerary</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="post-body-wrapper">
            <div class="post-content">
                <div class="copy-wrapper">
                    <h1 class="display">The title of the suggested itinerary</h1>
                    <h5 class="date">Day 1 - Crookwell > Laggan > Taralga > Curraweela > Wiarborough > Crookwell</h5>
                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row"><p>9:00am </p><span>-</span></th>
                                <td>Depart Crookwell Accommodation for Crystal Brook Lavender Farm</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>9:30am </p><span>-</span></th>
                                <td>Crystal Brook Lavender Farm</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>11:00am </p><span>-</span></th>
                                <td>Arrive in Taralga for Guided Town Tour</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>12:00pm </p><span>-</span></th>
                                <td>LUNCH</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>1:00pm </p><span>-</span></th>
                                <td>Depart for Tanjenong</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>1:30pm </p><span>-</span></th>
                                <td>Tanjenong Tin Shed Art Gallery/Coffee</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>2:30pm </p><span>-</span></th>
                                <td>Lochani Wines</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>3:30pm </p><span>-</span></th>
                                <td>Depart for Taralga</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>4:00pm </p><span>-</span></th>
                                <td>Taralga Berry Farm</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>5:30pm </p><span>-</span></th>
                                <td>Arrive back at accommodation in Crookwel</td>
                            </tr>
                        </tbody>
                    </table>
                    <h5 class="date">Day 1 - Crookwell > Laggan > Taralga > Curraweela > Wiarborough > Crookwell</h5>
                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row"><p>9:00am </p><span>-</span></th>
                                <td>Depart Crookwell Accommodation for Crystal Brook Lavender Farm</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>9:30am </p><span>-</span></th>
                                <td>Crystal Brook Lavender Farm</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>11:00am </p><span>-</span></th>
                                <td>Arrive in Taralga for Guided Town Tour</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>12:00pm </p><span>-</span></th>
                                <td>LUNCH</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>1:00pm </p><span>-</span></th>
                                <td>Depart for Tanjenong</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>1:30pm </p><span>-</span></th>
                                <td>Tanjenong Tin Shed Art Gallery/Coffee</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>2:30pm </p><span>-</span></th>
                                <td>Lochani Wines</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>3:30pm </p><span>-</span></th>
                                <td>Depart for Taralga</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>4:00pm </p><span>-</span></th>
                                <td>Taralga Berry Farm</td>
                            </tr>
                            <tr>
                                <th scope="row"><p>5:30pm </p><span>-</span></th>
                                <td>Arrive back at accommodation in Crookwel</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="galler-wrapper"></div>
                    <h5 class="display">Condimentum vitae sapien pellentesque</h5>
                    <p>In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta.</p>
                    <br>
                    <br>
                    <a href="#" class="theme-btn">Download Itinerary</a>
                </div>
            </div>
            <div class="post-sidebar">
                <div class="sidebar-item promo border-top-0">
                    <panel class="black-tag promo">
                        <div class="panel-header" style="background-image: url('assets/images/news-4.jpg');">
                            <div class="badge">
                                <img src="assets/images/icons/hot-deals.png" alt="hot-deals" clsss="img-fluid">
                            </div>
                        </div>
                        <div class="panel-body bg-theme-primary">
                            <div class="body-cat">
                                <p>Hot Deals</p>
                            </div>
                            <div class="body-promo">
                                <p class="discount">25 Off%</p>
                                <p>SPECIAL DEAL AT THIS LOREM IPSUM DOLOR ADIPISCING ELIT</p>
                            </div>
                            <div class="panel-footer">
                                <div class="see-more-wrapper">
                                    <a href="#">Get Deal
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </panel>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Featured Listing -->
<?php include("template-parts/partials/featured-listings.php");?>
<!-- Featured Listing: END -->

<?php include("template-parts/partials/newsletter.php");?>
<?php include("footer.php") ?>