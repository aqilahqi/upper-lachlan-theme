jQuery(document).ready(function () {

  if(jQuery(".dropdown-checkbox").length !== 0) {
    jQuery('input.dropdown-checkbox').on('change', function() {
        jQuery('input.dropdown-checkbox').not(this).prop('checked', false);  
    });
    jQuery('[data-toggle="datepicker"]').datepicker();
  }

  /* slider */
  if(jQuery("#home-news-slider").length !== 0) {
    jQuery('#home-news-slider').slick({
      dots: true,
      infinite: true,
      arrows: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    }); 
    
    jQuery('.news-left').click(function(){
      jQuery('#home-news-slider').slick('slickPrev');
    });
     
    jQuery('.news-right').click(function(){
      jQuery('#home-news-slider').slick('slickNext');
    });
  }     
  
  if(jQuery("#instagram-slider").length !== 0) {
    jQuery('#instagram-slider').slick({
      dots: false,
      infinite: true,
      arrows: false,
      // autoplay: true,
      // autoplaySpeed: 4000,
      slidesToShow: 4,
      slidesToScroll: 1,
      variableWidth: true,
      centerMode: true,
      responsive: [
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    }); 

    jQuery('.insta-left').click(function(){
      jQuery('#instagram-slider').slick('slickPrev');
    });
     
    jQuery('.insta-right').click(function(){
      jQuery('#instagram-slider').slick('slickNext');
    });
  }

  if(jQuery("#img-slider").length !== 0) {
    jQuery('#img-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      asNavFor: '#copy-slider',
      fade: true,
      cssEase: 'linear'
    });
  }

  if(jQuery("#copy-slider").length !== 0) {
    jQuery('#copy-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      arrows: false,
      asNavFor: '#img-slider',
      cssEase: 'linear',
      fade: true,
    });

    jQuery('.left').click(function(){
      jQuery('#copy-slider').slick('slickPrev');
     });
     
    jQuery('.right').click(function(){
      jQuery('#copy-slider').slick('slickNext');
     });
  }

  if(jQuery("#route-body").length !== 0) {
    jQuery('#route-body').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      speed: 500,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    jQuery('.left-drives').click(function(){
      jQuery('#route-body').slick('slickPrev');
    });
    
    jQuery('.right-drives').click(function(){
      jQuery('#route-body').slick('slickNext');
    });
  }

  if(jQuery("#attractions-slider").length !== 0) {
    jQuery('#attractions-slider').slick({
      dots: false,
      infinite: true,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 4000,
      slidesToShow: 5,
      slidesToScroll: 1,
      variableWidth: true,
      centerMode: true,
      responsive: [
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    }); 

    jQuery('.attractions-left').click(function(){
      jQuery('#attractions-slider').slick('slickPrev');
     });
     
    jQuery('.attractions-right').click(function(){
      jQuery('#attractions-slider').slick('slickNext');
     });
  }

  if(jQuery(".nav-img-slider").length !== 0) {
    jQuery('.nav-img-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    });
  
    jQuery('.img-slider-left').click(function(){
      jQuery('.nav-img-slider').slick('slickPrev');
    });
     
    jQuery('.img-slider-right').click(function(){
      jQuery('.nav-img-slider').slick('slickNext');
    });
  }

  if(jQuery('.home-slider').length !== 0) {
    jQuery('.home-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1600,
      arrows: false,
      dots: true,
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true
    });
  }

  if(jQuery('.description-slider-img').length !== 0) {
    jQuery('.description-slider-img').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      asNavFor: '.description-slider',
    });

    jQuery('.desc-left').click(function(){
      jQuery('.description-slider-img').slick('slickPrev');
    });
     
    jQuery('.desc-right').click(function(){
      jQuery('.description-slider-img').slick('slickNext');
    });

  }

  if(jQuery('.description-slider').length !== 0) {
    jQuery('.description-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      fade: true,
      cssEase: 'linear',
      adaptiveHeight: true,
      asNavFor: '.description-slider-img',
    });
  }

   jQuery('.body-item.wide .content-inner').each(function() {
    var $this = jQuery(this);
    if ($this.find('a.link-w-box').length > 9) { //if looking for direct descendants then do .children('div').length
        $this.addClass('divider');
    }
  });

  if(jQuery(".gallery-wrapper").length !== 0) {
    jQuery('.main-gallery').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.sub-gallery',
    });
    jQuery('.sub-gallery').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.main-gallery',
      dots: false,
      arrows: false,
      centerMode: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            centerPadding: '20px',
          }
        },
      ]
    });
  }

  jQuery( ".btn.btn-link[data-toggle='collapse']" ).click(function(e) {
    

    if (jQuery(window).width() > 768) {
      if(jQuery(this).attr('aria-expanded') == "true") {
        e.stopPropagation();
      }
    }

  });



  /** SELECT function */

  /* ----------- Upper Lachlan --------------- */
  jQuery('#collapseOne').on('hidden.bs.collapse', function () {
    jQuery('#Upper_Lachlan_Mouse_Over').removeClass('selected');
  });

  jQuery('#collapseOne').on('hide.bs.collapse', function () {
    jQuery('#Upper_Lachlan_Mouse_Over').removeClass('selected');
  });

  jQuery('#collapseOne').on('show.bs.collapse', function () {
    jQuery('#Upper_Lachlan_Mouse_Over').addClass('selected');
  });
  jQuery('#collapseOne').on('shown.bs.collapse', function () {
    jQuery('#Upper_Lachlan_Mouse_Over').addClass('selected');
  });

  /* --------------- Goulburn -------------------- */
  jQuery('#collapseTwo').on('hidden.bs.collapse', function () {
    jQuery('#Goulburn_Mouse_Over').removeClass('selected');

  });
  jQuery('#collapseTwo').on('hide.bs.collapse', function () {
    jQuery('#Goulburn_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseTwo').on('show.bs.collapse', function () {
    jQuery('#Goulburn_Mouse_Over').addClass('selected');
  });


  /* --------------------- Hilltops ----------------------- */
  jQuery('#collapseThree').on('hidden.bs.collapse', function () {
    jQuery('#Hilltops_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseThree').on('hide.bs.collapse', function () {
    jQuery('#Hilltops_Mouse_Over').removeClass('selected');
  });

  jQuery('#collapseThree').on('show.bs.collapse', function () {
    jQuery('#Hilltops_Mouse_Over').addClass('selected');
  });
  jQuery('#collapseThree').on('shown.bs.collapse', function () {
    jQuery('#Hilltops_Mouse_Over').addClass('selected');
  });

  /* ------------------- Yass ---------------------- */
  jQuery('#collapseFour').on('hidden.bs.collapse', function () {
    jQuery('#Yass_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseFour').on('hide.bs.collapse', function () {
    jQuery('#Yass_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseFour').on('show.bs.collapse', function () {
    jQuery('#Yass_Mouse_Over').addClass('selected');
  });

  /* --------------------- Quenbeyan ------------------------ */
  jQuery('#collapseFive').on('hidden.bs.collapse', function () {
    jQuery('#Queanbeyan_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseFive').on('hide.bs.collapse', function () {
    jQuery('#Queanbeyan_Mouse_Over').removeClass('selected');
  });

  jQuery('#collapseFive').on('show.bs.collapse', function () {
    jQuery('#Queanbeyan_Mouse_Over').addClass('selected');
  });

  jQuery('#collapseFive').on('shown.bs.collapse', function () {
    jQuery('#Queanbeyan_Mouse_Over').addClass('selected');
  });

  /* ------------------- Canberra ---------------------- */
  jQuery('#collapseSix').on('hidden.bs.collapse', function () {
    jQuery('#Canberra_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseSix').on('hide.bs.collapse', function () {
    jQuery('#Canberra_Mouse_Over').removeClass('selected');
  });
  jQuery('#collapseSix').on('show.bs.collapse', function () {
    jQuery('#Canberra_Mouse_Over').addClass('selected');
  });

  jQuery( "#Upper_Lachlan_Mouse_Over" ).click(function() {
    jQuery('#collapseOne').collapse('show');
  });

  jQuery( "#Goulburn_Mouse_Over" ).click(function() {
    jQuery('#collapseTwo').collapse('show');
  });

  jQuery( "#Hilltops_Mouse_Over" ).click(function() {
    jQuery('#collapseThree').collapse('show');
  });

  jQuery( "#Yass_Mouse_Over" ).click(function() {
    jQuery('#collapseFour').collapse('show');
  });

  jQuery( "#Queanbeyan_Mouse_Over" ).click(function() {
    jQuery('#collapseFive').collapse('show');
  });

  jQuery( "#Canberra_Mouse_Over" ).click(function() {
    jQuery('#collapseSix').collapse('show');
  });

});

