<?php include("header.php") ?>
<div class="page-wrapper d-lg-flex" style="background-image: url('assets/images/banner/about-history.jpg');">

    <div class="container align-self-end">
        <?php include("template-parts/partials/inner-page-banner.php");?>

        <!-- Breadcrumb -->
        <nav class="breadcrumb">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Breadcrumb</li>
            </ul>
        </nav>
        <!-- Breadcrumb: END -->
    </div>

</div>

<section class="inner-page">
    <div class="container">
        <!-- <div class="inner-page-header mx-auto">
            <h2 class="display">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et pharetra pharetra massa massa</h2>
        </div> -->
        <div class="inner-page-body pt-5">
            <div class="row">
                <div class="col-xs-12 col-md-6 order-md-1">
                    <!-- <img src="assets/images/about-live-post.png" alt="about-history" class="img-fluid"> -->
                    <div class="gallery-wrapper pt-0">
                        <div class="main-gallery">
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                        </div>
                        <div class="sub-gallery">
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <!-- <h3 class="display">Lorem ipsum dolor sit amet</h3> -->
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <br>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                </div>
            </div>
            <div class="row justify-content-end">
                <div class="share-post">
                    <div class="col-12">
                    <p>Share</p>
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="far fa-share-square"></i></a></li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="inner-page facts details bg-theme-light">
    <div class="container">
        <div class="section-header">
            <h2 class="display dark">Details</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6">
                <?php echo file_get_contents("assets/images/icons/phone.svg"); ?> 
                <p>PHONE</p>
                <h5 class="w-link"><a href="tel:0277778888">02 7777 8888</a></h5>
            </div>
            <div class="col-lg-4 col-md-6">
                <?php echo file_get_contents("assets/images/icons/Email.svg"); ?> 
                <p>EMAIL</p>
                <h5 class="w-link"><a href="mailto:info@somewhere.com.au">info@somewhere.com.au</a></h5>
            </div>
            <div class="col-lg-4 col-md-6">
                <?php echo file_get_contents("assets/images/icons/Website.svg"); ?> 
                <p>WEBSITE</p>
                <h5 class="w-link"><a href="www.somesiteinaustralia.com">www.somesiteinaustralia.com</a></h5>
            </div>
        </div>
    </div>
</section> -->

<section class="location">
    <div class="bg-theme-dark">
        <div class="section-header">
            <h2 class="display">Location</h2>
        </div>
    </div>
</section>

<section class="location-map">

    <div id="location-map"></div>
    <div class="direction-map bg-theme-light">
        <h4 class="display">Title od the Product Listing</h4>
        <p>Some Building or number, Some St, Some Suburb or Town NSW 2586</p>
        <a href="#" class="theme-btn">Click for directions</a>
    </div>
</section>

<script>
    // Initialize and add the map
    function initMap() {
    // The location of Uluru
    var uluru = {lat: -34.4574464, lng: 149.4676891};

    var faIcon = {
            path: 'M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z',
            fillColor: '#2B2F3D',
            fillOpacity: 1,
            scale: .1,
        stroke:0,
        anchor: new google.maps.Point(200, 480),
        };

    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('location-map'), {zoom: 15, center: uluru});
        
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: faIcon
        });
    }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChzMRpXQBROAjaEn9j_3DIxrx_Jvlfb08&callback=initMap"></script>
<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>