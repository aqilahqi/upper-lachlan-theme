<?php include("header.php") ?>

<section class="news news-post">
    <div class="container">
        <div class="post-header">
            <ul>
                <li><a href="#" class="theme-btn"><span>Events</span></a></li>
                <li>
                    <div class="navigation">
                        <ul>
                            <li><a href="#" class="left-nav"> Previous Post</a></li>
                            <li><a href="#" class="right-nav"> Next Post</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="post-body-wrapper">
            <div class="post-content">
                <div class="copy-wrapper">
                    <h5 class="date">20 march, 2018</h5>
                    <h1 class="display">The title of the news post, lorem ipsum dolor sit amet</h1>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque..</p>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit</p>
                    <div class="gallery-wrapper">
                        <div class="main-gallery">
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                            <div class="slider-main-item">
                                <div class="img-main-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </div>
                        </div>
                        <div class="sub-gallery">
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-1.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-2.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-3.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-4.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-5.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-6.png');"></div>
                            </div>
                            <div class="slider-sub-item">
                                <div class="img-sub-wrapper" style="background-image: url('assets/images/gallery-7.png');"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="author-wrapper">
                    <div class="author-img" style="background-image: url('assets/images/author.png');"></div>
                    <div class="author-details">
                        <p class="author-name">By <b>Bob Smith</b></p>
                        <p>Bob is an expert on all things Lorem ipsum dolor sit amet, pro aperiri perpetua id.</p>
                        <p>View all articles by <b>Bob Smith</b></p>
                    </div>
                </div>
            </div>
            <div class="post-sidebar">
                <div class="sidebar-item">
                    <ul class="sidebar-links">
                        <li class="title">Categories</li>
                        <li><a href="#">Eat &amp; Drink</a></li>
                        <li><a href="#">Events</a></li>
                        <li><a href="#">Retails</a></li>
                        <li><a href="#">Attractions</a></li>
                        <li><a href="#">Family</a></li>
                        <li><a href="#">People</a></li>
                        <li><a href="#">Festivals</a></li>
                    </ul>
                </div>
                <div class="sidebar-item">
                    <ul class="sidebar-links stories">
                        <li class="title">Recent News &amp; Stories</li>
                        <li><a href="#">Event Lorem ipsum dolor sit amet, usu no omnis.</a></li>
                        <li><a href="#">Event Lorem ipsum dolor sit amet, usu no omnis.</a></li>
                        <li><a href="#">Event Lorem ipsum dolor sit amet, usu no omnis.</a></li>
                        <li><a href="#">Event Lorem ipsum dolor sit amet, usu no omnis.</a></li>
                    </ul>
                </div>
                <div class="sidebar-item">
                    <ul class="sidebar-links">
                        <li class="title">Archives</li>
                        <li><a href="#">February 2018</a></li>
                        <li><a href="#">January 2018</a></li>
                        <li><a href="#">December 2017</a></li>
                        <li><a href="#">November 2017</a></li>
                    </ul>
                </div>
                <div class="sidebar-item promo">
                    <panel class="black-tag promo">
                        <div class="panel-header" style="background-image: url('assets/images/news-4.jpg');">
                            <div class="badge">
                                <img src="assets/images/icons/hot-deals.png" alt="hot-deals" clsss="img-fluid">
                            </div>
                        </div>
                        <div class="panel-body bg-theme-primary">
                            <div class="body-cat">
                                <p>Hot Deals</p>
                            </div>
                            <div class="body-promo">
                                <p class="discount">25 Off%</p>
                                <p>SPECIAL DEAL AT THIS LOREM IPSUM DOLOR ADIPISCING ELIT</p>
                            </div>
                            <div class="panel-footer">
                                <div class="see-more-wrapper">
                                    <a href="#">Get Deal
                                        <i class="fas fa-chevron-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </panel>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("template-parts/partials/newsletter.php");?>
<?php include("footer.php") ?>