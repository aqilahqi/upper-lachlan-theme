<?php include("header.php") ?>

<section class="inner-page search">
    <div class="container">
        <div class="inner-page-header mx-auto">
            <h2 class="display">Search Results for: Something from the website</h2>
        </div>
        <div class="inner-page-body">
            <div class="search-result-filter">
                <ul>
                    <li><button class="search-filter active">All(<span>4</span>)</button></li>
                    <li><button class="search-filter">Stay(<span>0</span>)</button></li>
                    <li><button class="search-filter">See &amp; Do(<span>0</span>)</button></li>
                    <li><button class="search-filter">Eat &amp; Drink(<span>0</span>)</button></li>
                    <li><button class="search-filter">Events(<span>0</span>)</button></li>
                    <li><button class="search-filter">News(<span>0</span>)</button></li>
                    <li><button class="search-filter">Others(<span>0</span>)</button></li>
                </ul>
            </div>
            <div class="search-results-wrapper">

                <!-- repeat item: START -->
                <div class="result-item-wrapper">
                    <div class="result-item">
                        <div class="result-cover" style="background-image:url('assets/images/search-1.jpg');"></div>
                        <div class="result-details">
                            <div class="copy-body">
                                <h2 class="display">This is the title of an ATDW Listing</h2>
                                <p>An excerpt of an ATDW listing will appear Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                            <div class="copy-footer"><a href="#" class="theme-btn">Find Out More</a></div>
                        </div>
                    </div>
                </div>
                <div class="result-item-wrapper">
                    <div class="result-item">
                        <div class="result-cover" style="background-image:url('assets/images/search-1.jpg');"></div>
                        <div class="result-details">
                            <div class="copy-body">
                                <h2 class="display">This is the title of an ATDW Listing</h2>
                                <p>An excerpt of an ATDW listing will appear Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                            <div class="copy-footer"><a href="#" class="theme-btn">Find Out More</a></div>
                        </div>
                    </div>
                </div>
                <div class="result-item-wrapper">
                    <div class="result-item">
                        <div class="result-cover" style="background-image:url('assets/images/search-1.jpg');"></div>
                        <div class="result-details">
                            <div class="copy-body">
                                <h2 class="display">This is the title of an ATDW Listing</h2>
                                <p>An excerpt of an ATDW listing will appear Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                            <div class="copy-footer"><a href="#" class="theme-btn">Find Out More</a></div>
                        </div>
                    </div>
                </div>
                <div class="result-item-wrapper">
                    <div class="result-item">
                        <div class="result-cover" style="background-image:url('assets/images/search-1.jpg');"></div>
                        <div class="result-details">
                            <div class="copy-body">
                                <h2 class="display">This is the title of an ATDW Listing</h2>
                                <p>An excerpt of an ATDW listing will appear Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                            <div class="copy-footer"><a href="#" class="theme-btn">Find Out More</a></div>
                        </div>
                    </div>
                </div>
                <!-- repeat item: END -->

            </div>
        </div>
    </div>
</section>

<!-- Newsletter section -->
<?php include("template-parts/partials/newsletter.php");?>
<!-- Newsletter section END -->
<?php include("footer.php") ?>