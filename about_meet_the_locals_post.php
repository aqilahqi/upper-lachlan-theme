<?php include("header.php") ?>
<?php include("template-parts/partials/post-page-banner.php");?>

<!-- Breadcrumb -->
<nav class="breadcrumb border-b">
    <div class="container">
        <ul>
            <li><a href="/">Home</a></li>
            <li class="active">Breadcrumb</li>
        </ul>
    </div>
</nav>
<!-- Breadcrumb: END -->

<section class="post-page">
    <div class="container">
        <div class="post-header">
            <h1 class="display">The name or names of the locals</h1>
            <p>The name of the business lorem ipsum dolor sit amet consectetur</p>
        </div>
        <div class="post-body">
            <div class="row">
                <div class="col-xs-12 col-lg-6 order-lg-2">
                    <img src="assets/images/post-page.png" alt="" class="img-fluid">
                </div>
                <div class="col-xs-12 col-lg-6">
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Egestas purus viverra accumsan in nisl. Metus dictum at tempor commodo ullamcorper a. In fermentum posuere urna nec tincidunt praesent semper. Nec ultrices dui sapien eget mi. Aliquam sem et tortor consequat. Quisque egestas diam in arcu cursus euismod quis viverra nibh. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <p>At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Cras fermentum odio eu feugiat pretium. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Varius morbi enim nunc faucibus a pellentesque. Aliquam etiam erat velit. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Fermentum leo vel orci porta non pulvinar neque.</p>
                    <!-- <div class="post-footer my-3">
                        <a href="#" class="theme-btn light">visit website</a>
                        <a href="#" class="theme-btn">meet more locals</a>
                    </div> -->
                    <ul>
                        <li><a href="#" class="theme-btn light">visit website</a></li>
                        <li><a href="#" class="theme-btn">meet more locals</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("template-parts/partials/newsletter.php");?>
<?php include("footer.php") ?>